<?php

/**
 * @file
 * Hook implementations for the gk_menu_price_band(_type) entities.
 */

/**
 * Implements hook_permission().
 */
function _gk_menu_price_band_permission() {
  return array(
    'administer gk_menu_price_band' => array(
      'title' => t('Administer GK menu price band entities'),
    ),
  );
}

/**
 * Implements hook_entity_info().
 */
function _gk_menu_price_band_entity_info() {
  $entity_info = array(
    'gk_menu_price_band' => array(
      'label' => t('Menu Price Band'),
      'entity class' => 'GKMenuPriceBand',
      'controller class' => 'GKMenuPriceBandController',
      'base table' => 'gk_menu_price_band',
      'fieldable' => TRUE,
      'load hook' => 'gk_menu_price_band_load',
      'uri callback' => 'entity_class_uri',
      'label callback' => 'entity_class_label',
      'access callback' => 'gk_menu_price_band_access',
      'module' => 'gk_menus',
      'entity keys' => array(
        'id' => 'mpbid',
        'bundle' => 'type',
        'label' => 'title',
      ),
      'bundles' => array(),
      'bundle keys' => array(
        'bundle' => 'type',
      ),
      'view modes' => array(
        'full' => array(
          'label' => t('Full'),
          'custom settings' => TRUE,
        ),
        'teaser' => array(
          'label' => t('Teaser'),
          'custom settings' => TRUE,
        ),
      ),
      'admin ui' => array(
        'path' => 'admin/content/gk-menus/menu-price-bands',
        'file' => 'includes/gk_menu_price_band/gk_menu_price_band.entity.admin.inc',
        'controller class' => 'GKMenuPriceBandAdminUIController',
      ),
    ),
    'gk_menu_price_band_type' => array(
      'label' => t('Menu Price Band Type'),
      'entity class' => 'GKEntityType',
      'controller class' => 'EntityAPIControllerExportable',
      'base table' => 'gk_menu_price_band_type',
      'fieldable' => FALSE,
      'exportable' => TRUE,
      'bundle of' => 'gk_menu_price_band',
      'access callback' => 'gk_menu_price_band_type_access',
      'module' => 'gk_menus',
      'entity keys' => array(
        'id' => 'id',
        'name' => 'type',
        'label' => 'label',
      ),
      'admin ui' => array(
        'path' => 'admin/structure/gk-menus/menu-price-band-types',
        'file' => 'includes/gk_menu_price_band/gk_menu_price_band.entity.admin.inc',
      ),
    ),
  );

  return $entity_info;
}

/**
 * Implements hook_entity_info_alter().
 */
function _gk_menu_price_band_entity_info_alter(&$entity_info) {
  foreach (gk_menu_price_band_type_list() as $type => $info) {
    $entity_info['gk_menu_price_band']['bundles'][$type] = array(
      'label' => $info->label,
      'admin' => array(
        'bundle argument' => 5,
        'path' => 'admin/structure/gk-menus/menu-price-band-types/manage/%gk_menu_price_band_type',
        'real path' => 'admin/structure/gk-menus/menu-price-band-types/manage/' . $type,
        'access arguments' => array('administer gk menus'),
      ),
    );
  }
}

/**
 * Implements hook_gk_menu_insert().
 */
function _gk_menu_price_band_gk_menu_insert(GKMenu $gk_menu) {
  if (variable_get('gk_menus_automatic_price_band_creation')) {
    // Create and save a new meta data entity.
    $gk_menu_meta_data = entity_create('gk_menu_meta_data', array(
      'type' => 'gk_menu_meta_data_type__default',
    ));

    entity_save('gk_menu_meta_data', $gk_menu_meta_data);

    if (!empty($gk_menu_meta_data->mmdid)) {
      // Create and save a new price band entity.
      $gk_menu_price_band = entity_create('gk_menu_price_band', array(
        'type' => 'gk_menu_price_band_type__default',
        'mid' => $gk_menu->mid,
        'title' => $gk_menu->title,
        'mmdid' => $gk_menu_meta_data->mmdid,
      ));

      entity_save('gk_menu_price_band', $gk_menu_price_band);
    }
  }
}
