<?php

/**
 * @file
 * API functions for the gk_menu_price_band(_type) entities.
 */

/**
 * Entity: gk_menu_price_band ==================================================
 */

/**
 * List of gk_menu_price_band entities.
 */
function gk_menu_price_band_list($mid = NULL) {
  $query = db_select('gk_menu_price_band', 'mpb')->fields('mpb', array('mpbid'));

  if (isset($mid)) {
    $query->condition('mpb.mid', $mid);
  }

  if ($mpbids = $query->execute()->fetchCol()) {
    return gk_menu_price_band_load_multiple($mpbids);
  }
}

/**
 * Entity access callback: gk_menu_price_band
 */
function gk_menu_price_band_access($op, $gk_menu_price_band, $account = NULL, $entity_type = NULL) {
  if (!isset($account)) {
    $account = $GLOBALS['user'];
  }

  return user_access('administer gk menus content', $account)
    && user_access('administer gk_menu_price_band', $account);
}

/**
 * Entity view callback: gk_menu_price_band
 */
function gk_menu_price_band_view($gk_menu_price_band, $view_mode = 'full', $langcode = NULL, $page = NULL) {
  return entity_view('gk_menu_price_band', array($gk_menu_price_band), $view_mode, $langcode, $page);
}

/**
 * Entity load callback: gk_menu_price_band
 */
function gk_menu_price_band_load($mpbid, $reset = FALSE) {
  $gk_menu_price_bands = gk_menu_price_band_load_multiple(array($mpbid), array(), $reset);
  return reset($gk_menu_price_bands);
}

/**
 * Entity load multiple callback: gk_menu_price_band
 */
function gk_menu_price_band_load_multiple($mpbids = array(), $conditions = array(), $reset = FALSE) {
  return entity_load('gk_menu_price_band', $mpbids, $conditions, $reset);
}

/**
 * Entity: gk_menu_price_band_type  ============================================
 */

/**
 * List of gk_menu_price_band_type entities.
 */
function gk_menu_price_band_type_list($type_name = NULL) {
  $types = entity_load_multiple_by_name('gk_menu_price_band_type', isset($type_name) ? array($type_name) : FALSE);
  return isset($type_name) ? reset($types) : $types;
}

/**
 * Entity access callback (used by admin UI): gk_menu_price_band_type
 */
function gk_menu_price_band_type_access($op, $entity = NULL, $account = NULL) {
  return user_access('administer gk menus', $account);
}

/**
 * Entity load callback: gk_menu_price_band_type
 */
function gk_menu_price_band_type_load($gk_menu_price_band_type) {
  return gk_menu_price_band_type_list($gk_menu_price_band_type);
}
