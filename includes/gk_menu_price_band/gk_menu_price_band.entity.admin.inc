<?php

/**
 * @file
 * Admin UI for the gk_menu_price_band(_type) entities.
 */

/**
 * Entity: gk_menu_price_band ==================================================
 */

/**
 * Entity form: gk_menu_price_band
 */
function gk_menu_price_band_form($form, &$form_state, GKMenuPriceBand $gk_menu_price_band) {
  $form_state['gk_menu_price_band'] = $gk_menu_price_band;
  $entity_id = entity_id('gk_menu_price_band', $gk_menu_price_band);

  $form['title'] = array(
    '#type' => 'textfield',
    '#title' => t('Name'),
    '#required' => TRUE,
    '#default_value' => $gk_menu_price_band->title,
  );

  // Field API form elements.
  field_attach_form('gk_menu_price_band', $gk_menu_price_band, $form, $form_state);

  $form['uid'] = array(
    '#type' => 'value',
    '#value' => $gk_menu_price_band->uid,
  );

  //
  // Menu.
  //
  $form['menu'] = array(
    '#type' => 'fieldset',
    '#title' => 'Menu',
    '#group' => 'additional_settings',
  );

  $mid_options = array();

  if (!$entity_id) {
    $mid_options[] = '<Select a menu>';
  }

  if ($gk_menus = gk_menu_list()) {
    foreach ($gk_menus as $gk_menu) {
      $mid_options[$gk_menu->mid] = $gk_menu->title;
    }
  }

  // ID.
  $form['menu']['mid'] = array(
    '#type' => 'select',
    '#title' => 'Menu',
    '#options' => $mid_options,
    '#default_value' => $entity_id ? $gk_menu_price_band->mid : NULL,
  );

  //
  // Menu meta data entity form.
  //
  $form['gk_menu_meta_data'] = array(
    '#type' => 'fieldset',
    '#title' => 'Meta data',
    '#group' => 'additional_settings',
    '#tree' => TRUE,
  );

  form_load_include($form_state, 'inc', 'gk_menus', 'includes/gk_menu_meta_data/gk_menu_meta_data.entity.admin');
  gk_menu_meta_data_embed_entity_form('gk_menu_price_band', $gk_menu_price_band, $form, $form_state);

  // Publishing options.
  $form['publishing_options'] = array(
    '#type' => 'fieldset',
    '#title' => 'Publishing options',
    '#group' => 'additional_settings',
  );

  $form['publishing_options']['status'] = array(
    '#type' => 'checkbox',
    '#title' => 'Published',
    '#default_value' => $gk_menu_price_band->status,
  );

  // Vertical tabs.
  $form['additional_settings'] = array(
    '#type' => 'vertical_tabs',
    '#weight' => 900,
  );

  // Actions.
  $form['actions'] = array(
    '#type' => 'actions',
    '#weight' => 1000,
  );

  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );

  return $form;
}

/**
 * Submit handler for the gk_menu_price_band entity form.
 */
function gk_menu_price_band_form_submit($form, &$form_state) {
  $gk_menu_price_band = $form_state['gk_menu_price_band'];

  entity_form_submit_build_entity('gk_menu_price_band', $gk_menu_price_band, $form, $form_state);
  entity_save('gk_menu_price_band', $gk_menu_price_band);

  $gk_menu_price_band_uri = entity_uri('gk_menu_price_band', $gk_menu_price_band);
  $form_state['redirect'] = $gk_menu_price_band_uri['path'];

  drupal_set_message(t('Menu price band %title saved.', array(
    '%title' => entity_label('gk_menu_price_band', $gk_menu_price_band),
  )));
}

/**
 * Entity: gk_menu_price_band_type =============================================
 */

/**
 * Entity form: gk_menu_price_band_type
 */
function gk_menu_price_band_type_form($form, &$form_state, $gk_menu_price_band_type, $op = 'edit') {
  if ($op == 'clone') {
    $gk_menu_price_band_type->label .= ' (cloned)';
    $gk_menu_price_band_type->type = '';
  }

  $form_state['gk_menu_price_band_type'] = $gk_menu_price_band_type;

  $form['label'] = array(
    '#title' => t('Label'),
    '#type' => 'textfield',
    '#default_value' => isset($gk_menu_price_band_type->label) ? $gk_menu_price_band_type->label : '',
    '#description' => t('The human-readable name of this type.'),
    '#required' => TRUE,
    '#size' => 30,
  );

  $form['type'] = array(
    '#type' => 'machine_name',
    '#default_value' => isset($gk_menu_price_band_type->type) ? $gk_menu_price_band_type->type : '',
    '#maxlength' => 32,
    '#disabled' => $gk_menu_price_band_type->isLocked() && $op != 'clone',
    '#machine_name' => array(
      'exists' => 'gk_menu_price_band_type_list',
      'source' => array('label'),
    ),
    '#description' => t('A unique machine-readable name for this type. It must only contain lowercase letters, numbers, and underscores.'),
  );

  $form['description'] = array(
    '#type' => 'textarea',
    '#title' => t('Description'),
    '#default_value' => isset($gk_menu_price_band_type->description) ? $gk_menu_price_band_type->description : '',
    '#description' => t('A description of this type.'),
  );

  // Actions.
  $form['actions'] = array(
    '#type' => 'actions',
    '#weight' => 1000,
  );

  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );

  return $form;
}

/**
 * Submit handler for the gk_menu_price_band_type entity form.
 */
function gk_menu_price_band_type_form_submit(&$form, &$form_state) {
  $gk_menu_price_band_type = entity_ui_form_submit_build_entity($form, $form_state);
  entity_save('gk_menu_price_band_type', $gk_menu_price_band_type);

  // Redirect the user back to the list of gk_menu_price_band_type entities.
  $form_state['redirect'] = 'admin/structure/gk-menus/menu-price-band-types';
}
