<?php

/**
 * @file
 * Controller classes for the gk_menu_price_band(_type) entities.
 */

/**
 * Entity controller class: gk_menu_price_band
 */
class GKMenuPriceBandController extends GKEntityBundleableController {
  /**
   * Overridden to delete associated gk_menu_meta_data entities.
   *
   * @see EntityAPIController::delete()
   */
  public function delete($ids, DatabaseTransaction $transaction = NULL) {
    $gk_menus = $ids ? $this->load($ids) : FALSE;

    if ($gk_menus) {
      $mmdids = array();

      foreach ($gk_menus as $gk_menu) {
        $mmdids[] = $gk_menu->mmdid;
      }

      entity_get_controller('gk_menu_meta_data')->delete($mmdids);
    }

    parent::delete($ids, $transaction);
  }
}

/**
 * Admin UI controller class for gk_menu_price_band entities.
 */
class GKMenuPriceBandAdminUIController extends GKEntityBundleableUIController {
  protected function overviewTableHeaders() {
    $headers = parent::overviewTableHeaders();

    // Add a column for the menu this price-band belongs to.
    $headers['mid'] = array(
      'label' => 'Menu',
      'weight' => 5,
    );

    return $headers;
  }

  protected function overviewTableFieldHandler($field, $entity) {
    $value = parent::overviewTableFieldHandler($field, $entity);

    // Override output of the 'mid' column.
    if ($field == 'mid') {
      if ($gk_menu = gk_menu_load($entity->mid)) {
        $value = l($gk_menu->title, 'gk-menu/' . $entity->mid);
      }
      else {
        $value = '<em>Missing/deleted (ID: ' . $entity->mid . ')</em>';
      }
    }

    return $value;
  }
}
