<?php

/**
 * @file
 * Entity classes for the gk_menu_price_band(_type) entities.
 */

/**
 * Entity class: gk_menu_price_band
 */
class GKMenuPriceBand extends Entity implements GKMenusHasMenuEntityInterface, GKMenusHasMetaDataEntityInterface {
  /**
   *
   */
  protected function defaultUri() {
    return array(
      'path' => 'admin/content/gk-menus/menu-price-bands/' . $this->mpbid,
    );
  }

  /**
   * Implements GKMenusHasMenuEntityInterface::getMenuEntity().
   */
  public function getMenuEntity() {
    if (empty($this->is_new) || !empty($this->mid)) {
      return gk_menu_load($this->mid);
    }
  }

  /**
   * Implements GKMenusHasMetaDataEntityInterface::getMetaDataEntity().
   */
  public function getMetaDataEntity() {
    if (!empty($this->mmdid)) {
      return gk_menu_meta_data_load($this->mmdid);
    }
  }

  /**
   * Implements GKMenusHasMetaDataEntityInterface::getMetaDataParentEntity().
   */
  public function getMetaDataParentEntity() {
    return $this->getMenuEntity();
  }

  /**
   * Implements GKMenusHasMetaDataEntityInterface::getMetaData().
   */
  public function getMetaData() {
    if ($gk_menu_meta_data = $this->getMetaDataEntity()) {
      return GKMenuMetaData::getMetaData($gk_menu_meta_data, $this);
    }
  }
}
