<?php

/**
 * @file
 * Entity classes for the gk_menu_meta_data(_type) entities.
 */

/**
 * Entity class: gk_menu_meta_data
 */
class GKMenuMetaData extends Entity {
  /**
   *
   */
  protected function defaultUri() {
    return array(
      'path' => 'admin/content/gk-menus/menu-meta-data/' . $this->mmdid,
    );
  }

  /**
   *
   */
  protected function defaultLabel() {
    if (empty($this->is_new)) {
      return t('gk_menu_meta_data:!mmdid', array(
        '!mmdid' => $this->mmdid,
      ));
    }

    return 'Meta data';
  }

  /**
   * Helper method used to build an array of meta data. This method inspects the
   * 'inherit' property of the given entity to decide whether data from the host
   * entities parent entity should be included.
   *
   * @param $gk_menu_meta_data GKMenuMetaData
   *   The meta data entity from which to base the resultant meta data.
   *
   * @param $host_entity Mixed
   *   The entity that owns the given GKMenuMetaData entity.
   */
  public static function getMetaData(GKMenuMetaData $gk_menu_meta_data, $host_entity) {
    $meta_data = array();
    $check_parent = FALSE;

    // Get a list of fields on the meta data entity.
    $field_instances = field_info_instances('gk_menu_meta_data', $gk_menu_meta_data->type);

    foreach ($field_instances as $field_name => $field_instance) {
      if (!$field_items = field_get_items('gk_menu_meta_data', $gk_menu_meta_data, $field_name)) {
        // The field value is empty so make a note for later that we need to
        // check the parent entity (if the inherit property says so).
        if (!$check_parent && !empty($gk_menu_meta_data->inherit)) {
          $check_parent = TRUE;
        }

        continue;
      }

      // If there is only ever going to be a single value in this field then
      // remove the additional layer created by the 2D nature in $field_items.
      if (($field_info = field_info_field($field_name)) && $field_info['cardinality'] == 1) {
        $field_items = reset($field_items);
      }

      $meta_data[$field_name] = $field_items;
    }

    // Check the parent entity for meta data?
    if ($check_parent && $parent_entity = $host_entity->getMetaDataParentEntity()) {
      ($parent_meta_data = $parent_entity->getMetaData()) && $meta_data += $parent_meta_data;
    }

    return $meta_data;
  }
}
