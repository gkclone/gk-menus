<?php

/**
 * @file
 * Admin UI for the gk_menu_meta_data(_type) entities.
 */

/**
 * Entity: gk_menu_meta_data ===================================================
 */

/**
 * Entity form: gk_menu_meta_data
 */
function gk_menu_meta_data_form($form, &$form_state, GKMenuMetaData $gk_menu_meta_data) {
  $form_state['gk_menu_meta_data'] = $gk_menu_meta_data;

  // Inherit property.
  $form['inherit'] = array(
    '#type' => 'checkbox',
    '#title' => 'Inherit',
    '#default_value' => $gk_menu_meta_data->inherit,
    '#description' => 'Inherit values for fields that are left blank.',
    '#weight' => -10,
  );

  // Field API form elements.
  field_attach_form('gk_menu_meta_data', $gk_menu_meta_data, $form, $form_state);

  // Actions.
  if (!isset($form_state['gk_menu_meta_data_array_parents'])) {
    $form['actions'] = array(
      '#type' => 'actions',
      '#weight' => 1000,
    );

    $form['actions']['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Save'),
    );
  }

  return $form;
}

/**
 * Submit handler for the gk_menu_meta_data entity form.
 */
function gk_menu_meta_data_form_submit($form, &$form_state) {
  $gk_menu_meta_data = $form_state['gk_menu_meta_data'];

  entity_form_submit_build_entity('gk_menu_meta_data', $gk_menu_meta_data, $form, $form_state);
  entity_save('gk_menu_meta_data', $gk_menu_meta_data);

  $uri = entity_uri('gk_menu_meta_data', $gk_menu_meta_data);
  $form_state['redirect'] = $uri['path'];

  drupal_set_message(t('Menu item %title saved.', array(
    '%title' => entity_label('gk_menu_meta_data', $gk_menu_meta_data),
  )));
}

/**
 * Helper function that enables form builders to embed the gk_menu_meta_data
 * entity form.
 */
function gk_menu_meta_data_embed_entity_form($entity_type, $entity, &$form, &$form_state) {
  // Where do we want to nest the form in the greater form?
  if (empty($form_state['gk_menu_meta_data_array_parents'])) {
    $form_state['gk_menu_meta_data_array_parents'] = array('gk_menu_meta_data', 'embedded_form');
  }

  // Similarly, where do we want the values to be nested in the form state?
  if (empty($form_state['gk_menu_meta_data_parents'])) {
    $form_state['gk_menu_meta_data_parents'] = array('gk_menu_meta_data');
  }

  // Custom form entity builder to handle $entity->mmdid on submit.
  $form['#entity_builders'][] = 'gk_menu_meta_data_embedded_form_entity_builder';

  // Determine which meta data entity to use. If there's one proivided in the
  // form state then we'll use that over anything else. If we're editing an
  // existing parent entity then one might already exist. Else create one.
  $op = 'edit';

  if (isset($form_state['gk_menu_meta_data_embed_entity'])) {
    $gk_menu_meta_data = $form_state['gk_menu_meta_data_embed_entity'];
  }
  elseif (!empty($entity->is_new) || empty($entity->mmdid) || !$gk_menu_meta_data = gk_menu_meta_data_load($entity->mmdid)) {
    $op = 'add';

    $gk_menu_meta_data = entity_create('gk_menu_meta_data', array(
      'type' => 'gk_menu_meta_data_type__default',
    ));
  }

  // Build the meta data entity form and nest it in $form.
  $embedded_form = array(
    // Nest the meta data form values in the form state.
    '#parents' => $form_state['gk_menu_meta_data_parents'],
  );

  $embedded_form = gk_menu_meta_data_form($embedded_form, $form_state, $gk_menu_meta_data);
  drupal_array_set_nested_value($form, $form_state['gk_menu_meta_data_array_parents'], $embedded_form);
}

/**
 * Entity builder callback used by forms that embed the meta data entity form.
 *
 * @see entity_form_submit_build_entity()
 */
function gk_menu_meta_data_embedded_form_entity_builder($entity_type, $entity, $form, $form_state) {
  // Build and save the meta data entity.
  gk_menu_meta_data_entity_form_submit_build_entity($form_state['gk_menu_meta_data'], $form, $form_state);

  // Populate $entity->mmdid.
  if (isset($form_state['gk_menu_meta_data']->mmdid)) {
    $entity->mmdid = $form_state['gk_menu_meta_data']->mmdid;
  }
}

/**
 * Helper function used to build a meta data entity when it's form has been
 * embedded. Used similarly to entity_form_submit_build_entity(), but doesn't
 * operate on the outer forms $form_state.
 */
function gk_menu_meta_data_entity_form_submit_build_entity($gk_menu_meta_data, $form, $form_state) {
  // Locate the embedded entity form within the greater form and ask field API
  // to handle field element submissions.
  $gk_menu_meta_data_form = drupal_array_get_nested_value($form, $form_state['gk_menu_meta_data_array_parents']);
  field_attach_submit('gk_menu_meta_data', $gk_menu_meta_data, $gk_menu_meta_data_form, $form_state);

  // Locate the area of $form_state['values'] that corresponds to this embedded
  // entity form and use it to set property values in the meta data entity.
  $gk_menu_meta_data_values = drupal_array_get_nested_value($form_state['values'], $form_state['gk_menu_meta_data_parents']);
  $values_excluding_fields = array_diff_key($gk_menu_meta_data_values, field_info_instances('gk_menu_meta_data', $gk_menu_meta_data->type));

  foreach ($values_excluding_fields as $key => $value) {
    $gk_menu_meta_data->$key = $value;
  }

  // Save the meta data entity.
  entity_save('gk_menu_meta_data', $gk_menu_meta_data);
}

/**
 * Entity: gk_menu_meta_data_type ==============================================
 */

/**
 * Entity form: gk_menu_meta_data_type
 */
function gk_menu_meta_data_type_form($form, &$form_state, $gk_menu_meta_data_type, $op = 'edit') {
  if ($op == 'clone') {
    $gk_menu_meta_data_type->label .= ' (cloned)';
    $gk_menu_meta_data_type->type = '';
  }

  $form_state['gk_menu_meta_data_type'] = $gk_menu_meta_data_type;

  $form['label'] = array(
    '#title' => t('Label'),
    '#type' => 'textfield',
    '#default_value' => isset($gk_menu_meta_data_type->label) ? $gk_menu_meta_data_type->label : '',
    '#description' => t('The human-readable name of this type.'),
    '#required' => TRUE,
    '#size' => 30,
  );

  $form['type'] = array(
    '#type' => 'machine_name',
    '#default_value' => isset($gk_menu_meta_data_type->type) ? $gk_menu_meta_data_type->type : '',
    '#maxlength' => 32,
    '#disabled' => $gk_menu_meta_data_type->isLocked() && $op != 'clone',
    '#machine_name' => array(
      'exists' => 'gk_menu_meta_data_type_list',
      'source' => array('label'),
    ),
    '#description' => t('A unique machine-readable name for this type. It must only contain lowercase letters, numbers, and underscores.'),
  );

  $form['description'] = array(
    '#type' => 'textarea',
    '#title' => t('Description'),
    '#default_value' => isset($gk_menu_meta_data_type->description) ? $gk_menu_meta_data_type->description : '',
    '#description' => t('A description of this type.'),
  );

  // Actions.
  $form['actions'] = array(
    '#type' => 'actions',
    '#weight' => 1000,
  );

  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );

  return $form;
}

/**
 * Submit handler for the gk_menu_meta_data_type entity form.
 */
function gk_menu_meta_data_type_form_submit(&$form, &$form_state) {
  $gk_menu_meta_data_type = entity_ui_form_submit_build_entity($form, $form_state);
  entity_save('gk_menu_meta_data_type', $gk_menu_meta_data_type);

  // Redirect the user back to the list of gk_menu_meta_data_type entities.
  $form_state['redirect'] = 'admin/structure/gk-menus/menu-meta-data-types';
}
