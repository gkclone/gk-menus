<?php

/**
 * @file
 * Hook implementations for the gk_menu_meta_data(_type) entities.
 */

/**
 * Implements hook_permission().
 */
function _gk_menu_meta_data_permission() {
  return array(
    'administer gk_menu_meta_data' => array(
      'title' => t('Administer GK menu meta data entities'),
    ),
  );
}

/**
 * Implements hook_entity_info().
 */
function _gk_menu_meta_data_entity_info() {
  $entity_info = array(
    'gk_menu_meta_data' => array(
      'label' => t('Menu Meta Data'),
      'plural label' => t('Menu Meta Data'),
      'entity class' => 'GKMenuMetaData',
      'controller class' => 'GKMenuMetaDataController',
      'base table' => 'gk_menu_meta_data',
      'fieldable' => TRUE,
      'load hook' => 'gk_menu_meta_data_load',
      'uri callback' => 'entity_class_uri',
      'label callback' => 'entity_class_label',
      'access callback' => 'gk_menu_meta_data_access',
      'module' => 'gk_menus',
      'entity keys' => array(
        'id' => 'mmdid',
        'bundle' => 'type',
      ),
      'bundles' => array(),
      'bundle keys' => array(
        'bundle' => 'type',
      ),
      'view modes' => array(
        'full' => array(
          'label' => t('Full'),
          'custom settings' => FALSE,
        ),
      ),
      'admin ui' => array(
        'path' => 'admin/content/gk-menus/menu-meta-data',
        'file' => 'includes/gk_menu_meta_data/gk_menu_meta_data.entity.admin.inc',
        'controller class' => 'GKMenuMetaDataAdminUIController',
      ),
    ),
    'gk_menu_meta_data_type' => array(
      'label' => t('Menu Meta Data Type'),
      'entity class' => 'GKEntityType',
      'controller class' => 'EntityAPIControllerExportable',
      'base table' => 'gk_menu_meta_data_type',
      'fieldable' => FALSE,
      'exportable' => TRUE,
      'bundle of' => 'gk_menu_meta_data',
      'access callback' => 'gk_menu_meta_data_type_access',
      'module' => 'gk_menus',
      'entity keys' => array(
        'id' => 'id',
        'name' => 'type',
        'label' => 'label',
      ),
      'admin ui' => array(
        'path' => 'admin/structure/gk-menus/menu-meta-data-types',
        'file' => 'includes/gk_menu_meta_data/gk_menu_meta_data.entity.admin.inc',
      ),
    ),
  );

  return $entity_info;
}

/**
 * Implements hook_entity_info_alter().
 */
function _gk_menu_meta_data_entity_info_alter(&$entity_info) {
  foreach (gk_menu_meta_data_type_list() as $type => $info) {
    $entity_info['gk_menu_meta_data']['bundles'][$type] = array(
      'label' => $info->label,
      'admin' => array(
        'bundle argument' => 5,
        'path' => 'admin/structure/gk-menus/menu-meta-data-types/manage/%gk_menu_meta_data_type',
        'real path' => 'admin/structure/gk-menus/menu-meta-data-types/manage/' . $type,
        'access arguments' => array('administer gk menus'),
      ),
    );
  }
}

/**
 * Implements hook_node_delete().
 */
function gk_menus_node_delete($node) {
  // Delete corresponding meta data entities.
  $query = db_select('gk_menu_price_band__node_location', 'pbnl')
    ->fields('pbnl')->condition('nid', $node->nid);

  if ($associations = $query->execute()->fetchAllAssoc('mmdid')) {
    entity_delete_multiple('gk_menu_meta_data', array_keys($associations));
  }
}

/**
 * Implements hook_gk_menu_meta_data_delete().
 */
function gk_menus_gk_menu_meta_data_delete($gk_menu_meta_data) {
  // Delete corresponding records from the associations table.
  db_delete('gk_menu_price_band__node_location')
    ->condition('mmdid', $gk_menu_meta_data->mmdid)
    ->execute();
}
