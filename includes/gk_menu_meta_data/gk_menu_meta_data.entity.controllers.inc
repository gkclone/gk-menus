<?php

/**
 * @file
 * Controller class for the gk_menu_meta_data entity.
 */

/**
 * Entity controller class: gk_menu_meta_data
 */
class GKMenuMetaDataController extends GKEntityBundleableController {
  public function create(array $values = array()) {
    $values += array(
      'inherit' => 1,
      'is_new' => TRUE,
      'created' => REQUEST_TIME,
      'changed' => REQUEST_TIME,
    );

    return new GKMenuMetaData($values, 'gk_menu_meta_data');
  }
}

/**
 * Admin UI controller class for gk_menu_meta_data entities.
 */
class GKMenuMetaDataAdminUIController extends GKEntityBundleableUIController {
  protected function overviewGetFilterableProperties() {
    // It should only be possible to filter meta data by the 'type' property.
    return array('type');
  }
}
