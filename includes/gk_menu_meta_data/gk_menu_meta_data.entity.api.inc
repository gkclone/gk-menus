<?php

/**
 * @file
 * API functions for the gk_menu_meta_data(_type) entities.
 */

/**
 * Entity: gk_menu_meta_data ===================================================
 */

/**
 * Entity access callback: gk_menu_meta_data
 */
function gk_menu_meta_data_access($op, $gk_menu_meta_data, $account = NULL, $entity_type = NULL) {
  if (!isset($account)) {
    $account = $GLOBALS['user'];
  }

  return user_access('administer gk menus content', $account)
    && user_access('administer gk_menu_meta_data', $account);
}

/**
 * Entity view callback: gk_menu_meta_data
 */
function gk_menu_meta_data_view($gk_menu_meta_data, $view_mode = 'full', $langcode = NULL, $page = NULL) {
  return entity_view('gk_menu_meta_data', array($gk_menu_meta_data), $view_mode, $langcode, $page);
}

/**
 * Entity load callback: gk_menu_meta_data
 */
function gk_menu_meta_data_load($mmdid, $reset = FALSE) {
  $gk_menu_meta_data = gk_menu_meta_data_load_multiple(array($mmdid), array(), $reset);
  return reset($gk_menu_meta_data);
}

/**
 * Entity load multiple callback: gk_menu_meta_data
 */
function gk_menu_meta_data_load_multiple($mmdids = array(), $conditions = array(), $reset = FALSE) {
  return entity_load('gk_menu_meta_data', $mmdids, $conditions, $reset);
}

/**
 * Entity: gk_menu_meta_data_type  =============================================
 */

/**
 * List of gk_menu_meta_data_type entities.
 */
function gk_menu_meta_data_type_list($type_name = NULL) {
  $types = entity_load_multiple_by_name('gk_menu_meta_data_type', isset($type_name) ? array($type_name) : FALSE);
  return isset($type_name) ? reset($types) : $types;
}

/**
 * Entity access callback (used by admin UI): gk_menu_meta_data_type
 */
function gk_menu_meta_data_type_access($op, $entity = NULL, $account = NULL) {
  return user_access('administer gk menus', $account);
}

/**
 * Entity load callback: gk_menu_meta_data_type
 */
function gk_menu_meta_data_type_load($gk_menu_meta_data_type) {
  return gk_menu_meta_data_type_list($gk_menu_meta_data_type);
}
