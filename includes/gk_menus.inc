<?php

/**
 * Return the names of the custom entities this module defines.
 */
function gk_menus_entities() {
  return array(
    'gk_menu',
    'gk_menu_item',
    'gk_menu_section',
    'gk_menu_price_band',
    'gk_menu_meta_data',
  );
}

/**
 * Invoke a subhook for one of this modules custom entities.
 *
 * @see gk_menus_entities()
 */
function gk_menus_subhook($hook, $args = array(), $merge_recursive = FALSE) {
  $return = array();

  foreach (gk_menus_entities() as $entity_type) {
    $subhook = "_{$entity_type}_$hook";

    if (function_exists($subhook)) {
      $return[] = call_user_func_array($subhook, $args);
    }
  }

  // Only merge/return data if the hook implementations returned anything.
  if ($return = array_filter($return)) {
    $merge_callback = $merge_recursive ? 'array_merge_recursive' : 'array_merge';
    return call_user_func_array($merge_callback, $return);
  }
}

/**
 * Invoke an alter subhook for one of this modules custom entities.
 *
 * @see gk_menus_entities()
 */
function gk_menus_subhook_alter($hook, $args) {
  foreach (gk_menus_entities() as $entity_type) {
    $subhook = "_{$entity_type}_{$hook}_alter";

    if (function_exists($subhook)) {
      call_user_func_array($subhook, $args);
    }
  }
}

/**
 * Get the active price band determined by the highest priority handler.
 *
 * @return
 *   Either a gk_menu_price_band entity or NULL if one could not be determined.
 */
function gk_menus_get_active_price_band(GKMenu $gk_menu) {
  // Cache the result of this function statically.
  static $entity = NULL;

  if (!isset($entity)) {
    // Get a list of handlers.
    $handlers = NULL;

    if ($cache = cache_get('gk_menus_active_price_band_handlers')) {
      $handlers = $cache->data;
    }
    else {
      // Build the list from scratch and cache it.
      $handlers = module_invoke_all('gk_menus_active_price_band_handlers');
      drupal_alter('gk_menus_active_price_band_handlers', $handlers);

      // Order the handlers by priority (highest first).
      uasort($handlers, function ($a, $b) {
        if ($a['priority'] == $b['priority']) {
          return 0;
        }

        return $a['priority'] < $b['priority'] ? 1 : -1;
      });

      cache_set('gk_menus_active_price_band_handlers', $handlers);
    }

    if ($handlers) {
      foreach ($handlers as $handler) {
        if (is_callable($handler['callback']) && $active_entity = $handler['callback']($gk_menu)) {
          $entity = $active_entity;
          break;
        }
      }
    }
  }

  return $entity;
}

/**
 * Get the active meta data determined by the highest priority handler.
 *
 * @return
 *   ...
 */
function gk_menus_get_active_meta_data(GKMenu $gk_menu) {
  // Cache the result of this function statically.
  static $meta_data = NULL;

  if (!isset($meta_data)) {
    // Get a list of handlers.
    $handlers = NULL;

    if ($cache = cache_get('gk_menus_active_meta_data_handlers')) {
      $handlers = $cache->data;
    }
    else {
      // Build the list from scratch and cache it.
      $handlers = module_invoke_all('gk_menus_active_meta_data_handlers');
      drupal_alter('gk_menus_active_meta_data_handlers', $handlers);

      // Order the handlers by priority (highest first).
      uasort($handlers, function ($a, $b) {
        if ($a['priority'] == $b['priority']) {
          return 0;
        }

        return $a['priority'] < $b['priority'] ? 1 : -1;
      });

      cache_set('gk_menus_active_meta_data_handlers', $handlers);
    }

    if ($handlers) {
      foreach ($handlers as $handler) {
        if (is_callable($handler['callback']) && ($active_meta_data = $handler['callback']($gk_menu)) !== NULL) {
          $meta_data = $active_meta_data;
          break;
        }
      }
    }
  }

  return $meta_data;
}

/**
 * Implements hook_gk_menus_active_meta_data_handlers().
 */
function gk_menus_gk_menus_active_meta_data_handlers() {
  return array(
    'menu' => array(
      'callback' => 'gk_menus_active_meta_data_callback__menu',
      'priority' => 0,
    ),
  );
}

/**
 * Active meta data handler callback: menu
 */
function gk_menus_active_meta_data_callback__menu(GKMenu $gk_menu) {
  return $gk_menu->getMetaData();
}
