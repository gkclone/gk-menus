<?php

/**
 * Settings form.
 */
function gk_menus_admin_settings_form($form, &$form_state) {
  // Options for price bands.
  $form['price_bands'] = array(
    '#type' => 'fieldset',
    '#title' => 'Price bands',
  );

  $form['price_bands']['gk_menus_automatic_price_band_creation'] = array(
    '#type' => 'checkbox',
    '#title' => 'Automatic creation',
    '#description' => 'Create a price band when a new menu is saved. The new price band will automatically be associated with the new menu.',
    '#default_value' => variable_get('gk_menus_automatic_price_band_creation', 0),
  );

  // Options for caching menu output.
  $form['caching'] = array(
    '#type' => 'fieldset',
    '#title' => 'Caching',
  );

  $form['caching']['gk_menus_cache_enabled'] = array(
    '#type' => 'checkbox',
    '#title' => 'Enabled',
    '#default_value' => variable_get('gk_menus_cache_enabled', 1),
  );

  $form['caching']['gk_menus_cache_debug'] = array(
    '#type' => 'checkbox',
    '#title' => 'Debug mode',
    '#default_value' => variable_get('gk_menus_cache_debug', 0),
    '#states' => array(
      'visible' => array(
        'input[name="gk_menus_cache_enabled"]' => array(
          'checked' => TRUE,
        ),
      ),
    ),
  );

  return system_settings_form($form);
}

/**
 * Settings form for the menu redirect option.
 */
function gk_menus_menu_redirect_admin_settings_form($form, &$form_state) {
  // Make it possible to select the menu entity to redirect to when the /menus
  // path is requested.
  $mid_options = array('<No redirect>');

  if ($gk_menus = gk_menu_list()) {
    foreach ($gk_menus as $gk_menu) {
      $mid_options[$gk_menu->mid] = $gk_menu->title;
    }
  }

  $form['gk_menus_redirect_path_mid'] = array(
    '#type' => 'select',
    '#title' => 'Menu redirect',
    '#description' => 'If set then the "/menus" path will be redirected to the chosen menu. If left unset then it will be possible to alias content at this path instead.',
    '#options' => $mid_options,
    '#default_value' => variable_get('gk_menus_redirect_path_mid', 0),
  );

  $form['#submit'][] = 'gk_menus_menu_redirect_admin_settings_form_submit';
  return system_settings_form($form);
}

/**
 * Submit handler for the settings form.
 */
function gk_menus_menu_redirect_admin_settings_form_submit($form, &$form_state) {
  // Rebuild the menus in order to refresh the status of the /menus router item.
  // Only do the rebuild if the value of the mid value actually changed from a
  // non-zero value to zero, or vice versa.
  $mid_default = $form['gk_menus_redirect_path_mid']['#default_value'];
  $mid_value = $form_state['values']['gk_menus_redirect_path_mid'];

  if ((!$mid_default && $mid_value) || ($mid_default && !$mid_value)) {
    variable_set('menu_rebuild_needed', TRUE);
  }
}
