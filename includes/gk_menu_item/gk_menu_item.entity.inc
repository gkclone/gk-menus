<?php

/**
 * @file
 * Entity classes for the gk_menu_item(_type) entities.
 */

/**
 * Entity class: gk_menu_item
 */
class GKMenuItem extends Entity implements GKMenusHasMenuEntityInterface {
  /**
   * A cache store for return values of the getMenuItemAssociations() method.
   */
  private static $cache__getMenuItemAssociations;

  /**
   *
   */
  protected function defaultUri() {
    return array(
      'path' => 'admin/content/gk-menus/menu-items/' . $this->miid,
    );
  }

  /**
   * Implements GKMenusHasMenuEntityInterface::getMenuEntity().
   */
  public function getMenuEntity() {
    if (empty($this->is_new)) {
      // Other menu entities (section, price band) have a property 'mid'. Could
      // be useful to have a similar property for menu items. So if it's not
      // already been set then we'll set it here.
      if (!isset($this->mid)) {
        $query = db_select('gk_menu_section', 's')->fields('s', array('mid'));
        $query->join('gk_menu_section__gk_menu_item', 'si', 's.msid = si.msid');
        $query->condition('si.miid', $this->miid);

        $this->mid = $query->execute()->fetchField();
      }

      if ($this->mid) {
        return gk_menu_load($this->mid);
      }
    }
  }

  /**
   * Retrieve information about this item's menu, section, and price bands.
   */
  public function getMenuItemInfo() {
    if (!empty($this->is_new)) {
      return NULL;
    }

    $menu_info = array();

    // Load the menu.
    $menu_info['menu']['entity'] = $this->getMenuEntity();

    // Load the section.
    if ($items__sections = self::getMenuItemAssociations('section', array('miid' => $this->miid), 'msid')) {
      $item__section = reset($items__sections);

      if ($gk_menu_section = gk_menu_section_load($item__section->msid)) {
        $menu_info['section'] = array(
          'entity' => $gk_menu_section,
          'meta' => (array) $item__section,
        );
      }
    }

    // Load the price bands.
    if ($items__price_bands = self::getMenuItemAssociations('price_band', array('miid' => $this->miid), 'mpbid')) {
      if ($gk_menu_price_bands = gk_menu_price_band_load_multiple(array_keys($items__price_bands))) {
        $menu_info['price_bands'] = array();

        foreach ($gk_menu_price_bands as $gk_menu_price_band) {
          $menu_info['price_bands'][$gk_menu_price_band->mpbid] = array(
            'entity' => $gk_menu_price_band,
            'meta' => (array) $items__price_bands[$gk_menu_price_band->mpbid],
          );
        }
      }
    }

    return $menu_info;
  }

  /**
   * Load records from the section/price band join tables.
   *
   * @param $type String
   *   'section' or 'price_band'. Determines which type of association to load.
   *
   * @param $conditions Array
   *   Optional filters to be applied to the query. Useful keys are:
   *     - mid: Menu ID
   *     - miid: Item ID
   *     - msid: Section ID
   *     - mpbid: Price band ID
   *   Note: to make best use of static caching ensure the keys of this array
   *   are in the same order each time the method is invoked. E.g. don't call
   *   this with array('mid' => X, 'miid' => Y) and then again with the 'miid'
   *   before the 'mid'.
   *
   * @param $assoc_field String
   *   The name of a column in the join table. Used to key the list of
   *   association records.
   */
  public static function getMenuItemAssociations($type, $conditions = array(), $assoc_field = 'miid') {
    $cache_key = $type . ':' . serialize($conditions);

    if (!isset(self::$cache__getMenuItemAssociations[$cache_key])) {
      switch ($type) {
        case 'section':
          $query_info = array(
            'entity table' => 'gk_menu_section',
            'entity join predicate' => 'jointable.msid = entity.msid',
            'join table' => 'gk_menu_section__gk_menu_item',
          );
        break;

        case 'price_band':
          $query_info = array(
            'entity table' => 'gk_menu_price_band',
            'entity join predicate' => 'jointable.mpbid = entity.mpbid',
            'join table' => 'gk_menu_price_band__gk_menu_item',
          );
        break;

        default:
          return NULL;
      }

      // A lookup for the table aliases of possible fields in $conditions.
      static $field_table_aliases = array(
        'mid' => 'entity',
        'miid' => 'jointable',
        'msid' => 'entity',
        'mpbid' => 'entity',
      );

      $query = db_select($query_info['join table'], 'jointable')->fields('jointable');
      $query->join($query_info['entity table'], 'entity', $query_info['entity join predicate']);

      foreach ($conditions as $field_name => $field_value) {
        $query->condition("$field_table_aliases[$field_name].$field_name", $field_value);
      }

      self::$cache__getMenuItemAssociations[$cache_key] = $query->execute()->fetchAllAssoc($assoc_field);
    }

    return self::$cache__getMenuItemAssociations[$cache_key];
  }

  /**
   * Get this menu items associated taxonomy terms.
   */
  public function getTaxonomyTerms($vocabulary_machine_name = NULL) {
    $terms = array();

    if ($taxonomy_tree = self::getTaxonomyTree($vocabulary_machine_name)) {
      foreach (array('nutrition_info', 'attributes') as $vocabulary) {
        if ($field = field_get_items('gk_menu_item', $this, "field_menu_item_$vocabulary")) {
          foreach ($field as $tid) {
            $tid = $tid['tid'];

            if (isset($taxonomy_tree[$tid])) {
              $terms[$tid] = $taxonomy_tree[$tid];
            }
          }
        }
      }
    }

    return $terms;
  }

  /**
   * Helper method for retrieving a list of taxonomy terms that could be
   * associated with gk_menu_item entities.
   *
   * @param $vocabulary_machine_name String
   *   Optional. Machine name of the vocabulary to return terms for. Defaults to
   *   NULL, meaning all possible terms are retrieved.
   *
   * @return Array
   *   An array of taxonomy term entities keyed by term ID.
   */
  public static function getTaxonomyTree($vocabulary_machine_name = NULL) {
    static $tree = array();

    if (!isset($tree[$vocabulary_machine_name])) {
      $tree[$vocabulary_machine_name] = array();

      $vocabulary_machine_names = array(
        'nutrition_information' => 1, 'menu_item_attributes' => 1,
      );

      if (isset($vocabulary_machine_names[$vocabulary_machine_name])) {
        $vocabulary_machine_names = array(
          $vocabulary_machine_name => 1,
        );
      }

      foreach ($vocabulary_machine_names as $machine_name => $one) {
        $vocabulary = taxonomy_vocabulary_machine_name_load($machine_name);

        foreach (taxonomy_get_tree($vocabulary->vid) as $term) {
          $tree[$vocabulary_machine_name][$term->tid] = $term;
        }
      }
    }

    return $tree[$vocabulary_machine_name];
  }
}
