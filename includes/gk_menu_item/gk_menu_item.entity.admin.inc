<?php

/**
 * @file
 * Admin UI for the gk_menu_item(_type) entities.
 */

/**
 * Entity: gk_menu_item ========================================================
 */

/**
 * Entity form: gk_menu_item
 */
function gk_menu_item_form($form, &$form_state, GKMenuItem $gk_menu_item) {
  // Retrieve information about this item's menu, section, and price bands.
  $menu_info = $gk_menu_item->getMenuItemInfo();

  // Entity builder callback to handle $gk_menu_item->menu on submit.
  $form['#entity_builders'] = array('gk_menu_item_form_entity_builder');

  $form_state['gk_menu_item'] = $gk_menu_item;
  $entity_id = entity_id('gk_menu_item', $gk_menu_item);

  $form['title'] = array(
    '#type' => 'textfield',
    '#title' => t('Name'),
    '#required' => TRUE,
    '#default_value' => $gk_menu_item->title,
  );

  // Field API form elements.
  field_attach_form('gk_menu_item', $gk_menu_item, $form, $form_state);

  $form['uid'] = array(
    '#type' => 'value',
    '#value' => $gk_menu_item->uid,
  );

  //
  // Menu.
  //
  $form['menu'] = array(
    '#type' => 'fieldset',
    '#title' => 'Menu',
    '#group' => 'additional_settings',
    '#tree' => TRUE,
  );

  $form['menu']['ajax_wrapper'] = array(
    '#theme_wrappers' => array('container'),
    '#attributes' => array(
      'id' => 'ajax-wrapper--menu',
    ),
  );

  // Select field for choosing which menu this item belongs to.
  $default_mid = NULL;
  $mid_options = array();

  if (isset($form_state['values']['menu']['ajax_wrapper']['mid'])) {
    $default_mid = $form_state['values']['menu']['ajax_wrapper']['mid'];
  }
  elseif (isset($menu_info['menu']['entity']->mid)) {
    $default_mid = $menu_info['menu']['entity']->mid;
  }
  else {
    $mid_options[] = '<Select a menu>';
  }

  if ($gk_menus = gk_menu_list()) {
    foreach ($gk_menus as $gk_menu) {
      $mid_options[$gk_menu->mid] = $gk_menu->title;
    }
  }

  $form['menu']['ajax_wrapper']['mid'] = array(
    '#type' => 'select',
    '#title' => 'Menu',
    '#options' => $mid_options,
    '#default_value' => $default_mid,
    '#ajax' => array(
      'callback' => 'gk_menu_item_form_ajax_callback__menu',
      'wrapper' => 'ajax-wrapper--menu',
    ),
  );

  // Section.
  $form['menu']['ajax_wrapper']['section'] = array(
    '#type' => 'fieldset',
    '#title' => 'Section',
    '#collapsible' => TRUE,
    '#collapsed' => !$default_mid,
  );

  if (!$default_mid) {
    $form['menu']['ajax_wrapper']['section']['#description'] = 'Select a menu to see available sections.';
  }
  else {
    $default_msid = NULL;
    $msid_options = array();

    if (isset($menu_info['section']['entity']->msid)) {
      $default_msid = $menu_info['section']['entity']->msid;
    }

    // Get a list of sections associated with the chosen menu.
    if ($gk_menu_sections = gk_menu_section_list($default_mid)) {
      foreach ($gk_menu_sections as $gk_menu_section) {
        $msid_options[$gk_menu_section->msid] = $gk_menu_section->title;
      }
    }

    // ID.
    $form['menu']['ajax_wrapper']['section']['msid'] = array(
      '#type' => 'select',
      '#title' => 'Section',
      '#options' => $msid_options,
      '#default_value' => $default_msid,
    );

    // Weight.
    $default_weight = 0;

    if (isset($menu_info['section']['meta']['weight'])) {
      $default_weight = $menu_info['section']['meta']['weight'];
    }

    $form['menu']['ajax_wrapper']['section']['weight'] = array(
      '#type' => 'select',
      '#title' => 'Weight',
      '#options' => drupal_map_assoc(range(-50, +50)),
      '#default_value' => $default_weight,
    );
  }

  // Price bands.
  $form['menu']['ajax_wrapper']['price_bands'] = array(
    '#type' => 'fieldset',
    '#title' => 'Price bands',
    '#collapsible' => TRUE,
    '#collapsed' => !$default_mid,
  );

  if (!$default_mid) {
    $form['menu']['ajax_wrapper']['price_bands']['#description'] = 'Select a menu to see available price bands.';
  }
  else {
    // Get a list of price bands associated with the chosen menu.
    if (!$gk_menu_price_bands = gk_menu_price_band_list($default_mid)) {
      $form['menu']['ajax_wrapper']['price_bands']['#description'] = 'There are no price bands associated with the chosen menu.';
    }
    else {
      // For each price band we want a checkbox and corresponding textfield.
      foreach ($gk_menu_price_bands as $gk_menu_price_band) {
        $mpbid = $gk_menu_price_band->mpbid;
        $default_price = '';

        if ($checked = isset($menu_info['price_bands'][$mpbid])) {
          $default_price = $menu_info['price_bands'][$mpbid]['meta']['price'];
        }

        $form['menu']['ajax_wrapper']['price_bands'][$mpbid] = array(
          // Theme wrapper for the checkbox/textfield.
          '#theme_wrappers' => array('container'),
          '#attributes' => array(
            'class' => array('price-band'),
          ),
          // Checkbox for associating this item with this price band.
          'mpbid' => array(
            '#type' => 'checkbox',
            '#title' => $gk_menu_price_band->title,
            '#return_value' => $mpbid,
            '#default_value' => $checked,
          ),
          // Text field for specifying a price for this item on this price band.
          'price' => array(
            '#type' => 'textfield',
            '#default_value' => $default_price,
            '#states' => array(
              'visible' => array(
                ':input[name="menu[ajax_wrapper][price_bands][' . $mpbid . '][mpbid]"]' => array(
                  'checked' => TRUE,
                ),
              ),
            ),
          ),
        );
      }
    }
  }

  // Publishing options.
  $form['publishing_options'] = array(
    '#type' => 'fieldset',
    '#title' => 'Publishing options',
    '#group' => 'additional_settings',
  );

  $form['publishing_options']['status'] = array(
    '#type' => 'checkbox',
    '#title' => 'Published',
    '#default_value' => $gk_menu_item->status,
  );

  // Vertical tabs.
  $form['additional_settings'] = array(
    '#type' => 'vertical_tabs',
    '#weight' => 900,
  );

  // Actions.
  $form['actions'] = array(
    '#type' => 'actions',
    '#weight' => 1000,
  );

  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );

  return $form;
}

/**
 * AJAX callback for the menu select element on the gk_menu_item entity form.
 */
function gk_menu_item_form_ajax_callback__menu($form, $form_state) {
  return $form['menu']['ajax_wrapper'];
}

/**
 * Submit handler for the gk_menu_item entity form.
 */
function gk_menu_item_form_submit($form, &$form_state) {
  $gk_menu_item = $form_state['gk_menu_item'];

  entity_form_submit_build_entity('gk_menu_item', $gk_menu_item, $form, $form_state);
  entity_save('gk_menu_item', $gk_menu_item);

  $gk_menu_item_uri = entity_uri('gk_menu_item', $gk_menu_item);
  $form_state['redirect'] = $gk_menu_item_uri['path'];

  drupal_set_message(t('Menu item %title saved.', array(
    '%title' => entity_label('gk_menu_item', $gk_menu_item),
  )));
}

/**
 * Entity builder callback used by the gk_menu_item entity form.
 *
 * @see entity_form_submit_build_entity()
 */
function gk_menu_item_form_entity_builder($entity_type, GKMenuItem $gk_menu_item, $form, $form_state) {
  $gk_menu_item->menu = $form_state['values']['menu']['ajax_wrapper'];

  // Ensure we don't pass blank price band values to the entity save function.
  if (!empty($gk_menu_item->menu['price_bands'])) {
    foreach ($gk_menu_item->menu['price_bands'] as $mpbid => $price_band) {
      if (empty($price_band['mpbid'])) {
        unset($gk_menu_item->menu['price_bands'][$mpbid]);
      }
    }
  }
}

/**
 * Entity: gk_menu_item_type ===================================================
 */

/**
 * Entity form: gk_menu_item_type
 */
function gk_menu_item_type_form($form, &$form_state, $gk_menu_item_type, $op = 'edit') {
  if ($op == 'clone') {
    $gk_menu_item_type->label .= ' (cloned)';
    $gk_menu_item_type->type = '';
  }

  $form_state['gk_menu_item_type'] = $gk_menu_item_type;

  $form['label'] = array(
    '#title' => t('Label'),
    '#type' => 'textfield',
    '#default_value' => isset($gk_menu_item_type->label) ? $gk_menu_item_type->label : '',
    '#description' => t('The human-readable name of this type.'),
    '#required' => TRUE,
    '#size' => 30,
  );

  $form['type'] = array(
    '#type' => 'machine_name',
    '#default_value' => isset($gk_menu_item_type->type) ? $gk_menu_item_type->type : '',
    '#maxlength' => 32,
    '#disabled' => $gk_menu_item_type->isLocked() && $op != 'clone',
    '#machine_name' => array(
      'exists' => 'gk_menu_item_type_list',
      'source' => array('label'),
    ),
    '#description' => t('A unique machine-readable name for this type. It must only contain lowercase letters, numbers, and underscores.'),
  );

  $form['description'] = array(
    '#type' => 'textarea',
    '#title' => t('Description'),
    '#default_value' => isset($gk_menu_item_type->description) ? $gk_menu_item_type->description : '',
    '#description' => t('A description of this type.'),
  );

  // Actions.
  $form['actions'] = array(
    '#type' => 'actions',
    '#weight' => 1000,
  );

  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );

  return $form;
}

/**
 * Submit handler for the gk_menu_item_type entity form.
 */
function gk_menu_item_type_form_submit(&$form, &$form_state) {
  $gk_menu_item_type = entity_ui_form_submit_build_entity($form, $form_state);
  entity_save('gk_menu_item_type', $gk_menu_item_type);

  // Redirect the user back to the list of gk_menu_item_type entities.
  $form_state['redirect'] = 'admin/structure/gk-menus/menu-item-types';
}
