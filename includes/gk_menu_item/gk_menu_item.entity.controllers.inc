<?php

/**
 * @file
 * Controller classes for the gk_menu_item(_type) entities.
 */

/**
 * Entity controller class: gk_menu_item
 */
class GKMenuItemController extends GKEntityBundleableController {
  /**
   * Overridden to write to the gk_menu_item join tables and clear the cache.
   *
   * @see EntityAPIController::save()
   */
  public function save($gk_menu_item, DatabaseTransaction $transaction = NULL) {
    if ($return = parent::save($gk_menu_item, $transaction)) {
      // Write to the gk_menu_section__gk_menu_item table.
      if (!empty($gk_menu_item->menu['section'])) {
        db_merge('gk_menu_section__gk_menu_item')
          ->key(array(
            'miid' => $gk_menu_item->miid,
          ))
          ->fields(array(
            'msid' => $gk_menu_item->menu['section']['msid'],
            'weight' => $gk_menu_item->menu['section']['weight'],
          ))
          ->execute();
      }

      // Delete all price band data for this menu item and insert fresh.
      db_delete('gk_menu_price_band__gk_menu_item')
        ->condition('miid', $gk_menu_item->miid)
        ->execute();

      if (!empty($gk_menu_item->menu['price_bands'])) {
        foreach ($gk_menu_item->menu['price_bands'] as $price_band) {
          db_insert('gk_menu_price_band__gk_menu_item')
            ->fields(array(
              'miid' => $gk_menu_item->miid,
              'mpbid' => $price_band['mpbid'],
              'price' => $price_band['price'] ? $price_band['price'] : NULL,
            ))
            ->execute();
        }
      }

      // Clear the content builder cache for the menu this item belongs to.
      $query = db_select('gk_menu_section', 'ms')->fields('ms', array('mid'))
        ->condition('msmi.miid', $gk_menu_item->miid);

      $query->join('gk_menu_section__gk_menu_item', 'msmi', 'ms.msid = msmi.msid');

      if ($mid = $query->execute()->fetchField()) {
        GKMenu::flushCaches($mid);
      }
    }

    return $return;
  }

  /**
   * Overridden to delete records from the gk_menu_item join tables.
   *
   * @see EntityAPIController::delete()
   */
  public function delete($ids, DatabaseTransaction $transaction = NULL) {
    parent::delete($ids, $transaction);

    $tables = array(
      'gk_menu_section__gk_menu_item',
      'gk_menu_price_band__gk_menu_item',
    );

    foreach ($tables as $table) {
      db_delete($table)->condition('miid', $ids, 'IN')->execute();
    }
  }
}

/**
 * Admin UI controller class for gk_menu_item entities.
 */
class GKMenuItemAdminUIController extends GKEntityBundleableUIController {
  protected function overviewTableHeaders() {
    $headers = parent::overviewTableHeaders();

    // Add columns for menu, section, weight and price bands.
    $headers['mid'] = array(
      'label' => 'Menu',
      'weight' => 4,
    );

    $headers['msid'] = array(
      'label' => 'Section',
      'weight' => 5,
    );

    $headers['weight'] = array(
      'label' => 'weight',
      'weight' => 6,
    );

    $headers['mpbids'] = array(
      'label' => 'Price bands',
      'weight' => 7,
    );

    return $headers;
  }

  protected function overviewTableFieldHandler($field, $entity) {
    $value = parent::overviewTableFieldHandler($field, $entity);

    // Override output of the additional columns.
    if ($field == 'mid' || $field == 'msid' || $field == 'weight' || $field == 'mpbids') {
      $info = $entity->getMenuItemInfo();

      switch ($field) {
        case 'mid':
          if ($info['menu']['entity']) {
            $value = l($info['menu']['entity']->title, 'gk-menu/' . $entity->mid);
          }
          else {
            $value = '<em>Missing/deleted (ID: ' . $entity->mid . ')</em>';
          }
        break;

        case 'msid':
          if ($info['section']['entity']) {
            $uri = entity_uri('gk_menu_section', $info['section']['entity']);
            $value = l($info['section']['entity']->title, $uri['path']);
          }
        break;

        case 'weight':
          $value = $info['section']['meta']['weight'];
        break;

        case 'mpbids':
          $value = '<em>None</em>';

          if (!empty($info['price_bands'])) {
            $price_bands = array();

            foreach ($info['price_bands'] as $price_band) {
              $uri = entity_uri('gk_menu_price_band', $price_band['entity']);
              $link = l($price_band['entity']->title, $uri['path']);

              $price = $price_band['meta']['price'];
              $price = !empty($price) ? $price : '<em>No price set</em>';

              $price_bands[] = $link . ': ' . $price;
            }

            $value = implode('<br>', $price_bands);
          }
        break;
      }
    }

    return $value;
  }

  /**
   * Overridden to add a tag so that the query can be altered.
   *
   * @see _gk_menu_item_query_alter()
   * @see GKEntityBundleableUIController::overviewGetQuery()
   */
  protected function overviewGetQuery($conditions = array()) {
    $query = parent::overviewGetQuery($conditions);
    $query->addTag('GKMenuItemAdminUIController::overviewGetQuery');
    return $query;
  }
}
