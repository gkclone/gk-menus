<?php

/**
 * @file
 * API functions for the gk_menu_item(_type) entities.
 */

/**
 * Entity: gk_menu_item ========================================================
 */

/**
 * Entity access callback: gk_menu_item
 */
function gk_menu_item_access($op, $gk_menu_item, $account = NULL, $entity_type = NULL) {
  if (!isset($account)) {
    $account = $GLOBALS['user'];
  }

  return user_access('administer gk menus content', $account)
    && user_access('administer gk_menu_item', $account);
}

/**
 * Entity view callback: gk_menu_item
 */
function gk_menu_item_view($gk_menu_item, $view_mode = 'full', $langcode = NULL, $page = NULL) {
  return entity_view('gk_menu_item', array($gk_menu_item), $view_mode, $langcode, $page);
}

/**
 * Entity load callback: gk_menu_item
 */
function gk_menu_item_load($msid, $reset = FALSE) {
  $gk_menu_items = gk_menu_item_load_multiple(array($msid), array(), $reset);
  return reset($gk_menu_items);
}

/**
 * Entity load multiple callback: gk_menu_item
 */
function gk_menu_item_load_multiple($msids = array(), $conditions = array(), $reset = FALSE) {
  return entity_load('gk_menu_item', $msids, $conditions, $reset);
}

/**
 * Entity: gk_menu_item_type  ==================================================
 */

/**
 * List of gk_menu_item_type entities.
 */
function gk_menu_item_type_list($type_name = NULL) {
  $types = entity_load_multiple_by_name('gk_menu_item_type', isset($type_name) ? array($type_name) : FALSE);
  return isset($type_name) ? reset($types) : $types;
}

/**
 * Entity access callback (used by admin UI): gk_menu_item_type
 */
function gk_menu_item_type_access($op, $entity = NULL, $account = NULL) {
  return user_access('administer gk menus', $account);
}

/**
 * Entity load callback: gk_menu_item_type
 */
function gk_menu_item_type_load($gk_menu_item_type) {
  return gk_menu_item_type_list($gk_menu_item_type);
}
