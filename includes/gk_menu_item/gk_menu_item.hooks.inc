<?php

/**
 * @file
 * Hook implementations for the gk_menu_item(_type) entities.
 */

/**
 * Implements hook_permission().
 */
function _gk_menu_item_permission() {
  return array(
    'administer gk_menu_item' => array(
      'title' => t('Administer GK menu item entities'),
    ),
  );
}

/**
 * Implements hook_entity_info().
 */
function _gk_menu_item_entity_info() {
  $entity_info = array(
    'gk_menu_item' => array(
      'label' => t('Menu Item'),
      'entity class' => 'GKMenuItem',
      'controller class' => 'GKMenuItemController',
      'base table' => 'gk_menu_item',
      'fieldable' => TRUE,
      'load hook' => 'gk_menu_item_load',
      'uri callback' => 'entity_class_uri',
      'label callback' => 'entity_class_label',
      'access callback' => 'gk_menu_item_access',
      'module' => 'gk_menus',
      'entity keys' => array(
        'id' => 'miid',
        'bundle' => 'type',
        'label' => 'title',
      ),
      'bundles' => array(),
      'bundle keys' => array(
        'bundle' => 'type',
      ),
      'view modes' => array(
        'full' => array(
          'label' => t('Full'),
          'custom settings' => TRUE,
        ),
        'teaser' => array(
          'label' => t('Teaser'),
          'custom settings' => TRUE,
        ),
      ),
      'admin ui' => array(
        'path' => 'admin/content/gk-menus/menu-items',
        'file' => 'includes/gk_menu_item/gk_menu_item.entity.admin.inc',
        'controller class' => 'GKMenuItemAdminUIController',
      ),
    ),
    'gk_menu_item_type' => array(
      'label' => t('Menu Item Type'),
      'entity class' => 'GKEntityType',
      'controller class' => 'EntityAPIControllerExportable',
      'base table' => 'gk_menu_item_type',
      'fieldable' => FALSE,
      'exportable' => TRUE,
      'bundle of' => 'gk_menu_item',
      'access callback' => 'gk_menu_item_type_access',
      'module' => 'gk_menus',
      'entity keys' => array(
        'id' => 'id',
        'name' => 'type',
        'label' => 'label',
      ),
      'admin ui' => array(
        'path' => 'admin/structure/gk-menus/menu-item-types',
        'file' => 'includes/gk_menu_item/gk_menu_item.entity.admin.inc',
      ),
    ),
  );

  return $entity_info;
}

/**
 * Implements hook_entity_info_alter().
 */
function _gk_menu_item_entity_info_alter(&$entity_info) {
  foreach (gk_menu_item_type_list() as $type => $info) {
    $entity_info['gk_menu_item']['bundles'][$type] = array(
      'label' => $info->label,
      'admin' => array(
        'bundle argument' => 5,
        'path' => 'admin/structure/gk-menus/menu-item-types/manage/%gk_menu_item_type',
        'real path' => 'admin/structure/gk-menus/menu-item-types/manage/' . $type,
        'access arguments' => array('administer gk menus'),
      ),
    );
  }
}

/**
 * Implements hook_query_alter().
 */
function _gk_menu_item_query_alter(QueryAlterableInterface $query) {
  // Alter the admin overview query for gk_menu_item entities to apply custom
  // ordering based on weight properties. This will make it easier to manage
  // menu items because they will be grouped by menu and section.
  if ($query->hasTag('GKMenuItemAdminUIController::overviewGetQuery')) {
    // Remove any default ordering (i.e. by primary key).
    $fields =& $query->getOrderBy();
    $fields = array();

    // Join tables that contain weight properties for our entities.
    $query->join('gk_menu_section__gk_menu_item', 'si', 'gk_menu_item.miid = si.miid');
    $query->join('gk_menu_section', 's', 'si.msid = s.msid');
    $query->join('gk_menu', 'm', 's.mid = m.mid');

    // Order by menu weight, then by mid so that items belonging to menus with
    // equal weights are still separated.
    $query->orderBy('m.weight');
    $query->orderBy('m.mid');

    // Order by section weight, and similarly by msid so that items belonging
    // to sections with equal weights are still separated.
    $query->orderBy('s.weight');
    $query->orderBy('s.msid');

    // Finally, order by item weight.
    $query->orderBy('si.weight');
  }
}

/**
 * Implements hook_token_info().
 */
function _gk_menu_item_token_info() {
  return array(
    'types' => array(
      'gk-menu-item' => array(
        'name' => t('GK Menu Item'),
        'description' => t('Tokens for GK menu items.'),
      ),
    ),
    'tokens' => array(
      'gk-menu-item' => array(
        'title' => array(
          'name' => t('GK Menu Item title'),
          'description' => t('The title of the wGK menu item.'),
        ),
      ),
    ),
  );
}

/**
 * Implements hook_tokens().
 */
function _gk_menu_item_tokens($type, $tokens, array $data = array(), array $options = array()) {
  $replacements = array();

  if ($type == 'entity' && $data['entity_type'] == 'gk_menu_item') {
    foreach ($tokens as $name => $original) {
      switch ($name) {
        case 'title':
          $replacements[$original] = $data['entity']->title;
        break;
      }
    }
  }

  return $replacements;
}
