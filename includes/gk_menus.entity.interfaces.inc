<?php

/**
 *
 */
interface GKMenusHasMenuEntityInterface {
  /**
   * Retrieve the gk_menu entity associated with this entity.
   */
  public function getMenuEntity();
}

/**
 *
 */
interface GKMenusHasMetaDataEntityInterface {
  /**
   * Retrieve the gk_menu_meta_data entity associated with this entity.
   */
  public function getMetaDataEntity();

  /**
   * Retrieve this entities parent entity:
   *   - gk_menu_price_band => gk_menu
   *   - gk_menu => null
   */
  public function getMetaDataParentEntity();

  /**
   * Retrieve the actual meta data for this entity. This will involve cascading
   * from this entity to it's parent for data that this entity doesn't define.
   * For example, if this entity doesn't have a PDF file set in it's associated
   * meta data entity then this should look in the parent entities meta data and
   * so forth until a non-empty value is discovered.
   */
  public function getMetaData();
}
