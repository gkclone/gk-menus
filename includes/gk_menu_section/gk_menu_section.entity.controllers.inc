<?php

/**
 * @file
 * Controller classes for the gk_menu_section(_type) entities.
 */

/**
 * Entity controller class: gk_menu_section
 */
class GKMenuSectionController extends GKEntityBundleableController {
  /**
   * Overridden to clear the content builder cache for this sections menu.
   *
   * @see EntityAPIController::save()
   */
  public function save($gk_menu_section, DatabaseTransaction $transaction = NULL) {
    if ($return = parent::save($gk_menu_section, $transaction)) {
      GKMenu::flushCaches($gk_menu_section->mid);
    }

    return $return;
  }
}

/**
 * Admin UI controller class for gk_menu_section entities.
 */
class GKMenuSectionAdminUIController extends GKEntityBundleableUIController {
  protected function overviewTableHeaders() {
    $headers = parent::overviewTableHeaders();

    // Add a column for the menu this section belongs to.
    $headers['mid'] = array(
      'label' => 'Menu',
      'weight' => 4,
    );

    // Add a column for the weight of this section.
    $headers['weight'] = array(
      'label' => 'weight',
      'weight' => 5,
    );

    // Add a column for the parent section this section belongs to.
    $headers['parent'] = array(
      'label' => 'Parent',
      'weight' => 6,
    );

    return $headers;
  }

  protected function overviewTableFieldHandler($field, $entity) {
    $value = parent::overviewTableFieldHandler($field, $entity);

    // Override output of the 'mid' column.
    if ($field == 'mid') {
      if ($gk_menu = gk_menu_load($entity->mid)) {
        $value = l($gk_menu->title, 'gk-menu/' . $entity->mid);
      }
      else {
        $value = '<em>Missing/deleted (ID: ' . $entity->mid . ')</em>';
      }
    }
    elseif ($field == 'parent') {
      if (empty($entity->parent)) {
        $value = '<em>None</em>';
      }
      elseif ($gk_menu_section = gk_menu_section_load($entity->parent)) {
        $uri = entity_uri('gk_menu_section', $gk_menu_section);
        $value = l($gk_menu_section->title, $uri['path']);
      }
    }

    return $value;
  }

  protected function overviewGetQuery($conditions = array()) {
    $query = parent::overviewGetQuery($conditions);

    // Order the results by mid and weight rather than primary key.
    $query->order = array();
    $query->propertyOrderBy('mid');
    $query->propertyOrderBy('weight');

    return $query;
  }
}
