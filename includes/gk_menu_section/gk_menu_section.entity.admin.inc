<?php

/**
 * @file
 * Admin UI for the gk_menu_section(_type) entities.
 */

/**
 * Entity: gk_menu_section =====================================================
 */

/**
 * Entity form: gk_menu_section
 */
function gk_menu_section_form($form, &$form_state, GKMenuSection $gk_menu_section) {
  // Entity builder callback to handle entity properties on submit.
  $form['#entity_builders'] = array('gk_menu_section_form_entity_builder');

  $form_state['gk_menu_section'] = $gk_menu_section;
  $entity_id = entity_id('gk_menu_section', $gk_menu_section);

  $form['title'] = array(
    '#type' => 'textfield',
    '#title' => t('Name'),
    '#required' => TRUE,
    '#default_value' => $gk_menu_section->title,
    '#weight' => -10,
  );

  // Field API form elements.
  field_attach_form('gk_menu_section', $gk_menu_section, $form, $form_state);

  $form['uid'] = array(
    '#type' => 'value',
    '#value' => $gk_menu_section->uid,
  );

  //
  // Menu.
  //
  $form['menu'] = array(
    '#type' => 'fieldset',
    '#title' => 'Menu',
    '#group' => 'additional_settings',
    '#tree' => TRUE,
  );

  $form['menu']['ajax_wrapper'] = array(
    '#theme_wrappers' => array('container'),
    '#attributes' => array(
      'id' => 'ajax-wrapper--menu',
    ),
  );

  // Select field for choosing which menu this section belongs to.
  $default_mid = NULL;
  $mid_options = array();

  if (isset($form_state['values']['menu']['ajax_wrapper']['mid'])) {
    $default_mid = $form_state['values']['menu']['ajax_wrapper']['mid'];
  }
  elseif ($entity_id) {
    $default_mid = $gk_menu_section->mid;
  }
  else {
    $mid_options[] = '<Select a menu>';
  }

  if ($gk_menus = gk_menu_list()) {
    foreach ($gk_menus as $gk_menu) {
      $mid_options[$gk_menu->mid] = $gk_menu->title;
    }
  }

  $form['menu']['ajax_wrapper']['mid'] = array(
    '#type' => 'select',
    '#title' => 'Menu',
    '#options' => $mid_options,
    '#default_value' => $default_mid,
    '#ajax' => array(
      'callback' => 'gk_menu_section_form_ajax_callback__menu',
      'wrapper' => 'ajax-wrapper--menu',
    ),
  );

  // Parent section.
  $form['menu']['ajax_wrapper']['section'] = array(
    '#type' => 'fieldset',
    '#title' => 'Parent section',
    '#collapsible' => TRUE,
    '#collapsed' => !$default_mid,
  );

  if (!$default_mid) {
    $form['menu']['ajax_wrapper']['section']['#description'] = 'Select a menu to see available sections.';
  }
  else {
    // Get a list of sections associated with the chosen menu.
    $msid_options = array('<No parent>');

    if ($gk_menu_sections = gk_menu_section_list($default_mid)) {
      foreach ($gk_menu_sections as $entity) {
        // The parent menu section cannot be this section itself.
        if ($entity_id && $entity_id == $entity->msid) {
          continue;
        }

        $msid_options[$entity->msid] = $entity->title;
      }
    }

    // ID.
    $form['menu']['ajax_wrapper']['section']['msid'] = array(
      '#type' => 'select',
      '#title' => 'Section',
      '#options' => $msid_options,
      '#default_value' => $entity_id ? $gk_menu_section->parent : NULL,
    );

    // Weight.
    $form['menu']['ajax_wrapper']['section']['weight'] = array(
      '#type' => 'select',
      '#title' => 'Weight',
      '#options' => drupal_map_assoc(range(-50, +50)),
      '#default_value' => $entity_id ? $gk_menu_section->weight : 0,
    );
  }

  // Publishing options.
  $form['publishing_options'] = array(
    '#type' => 'fieldset',
    '#title' => 'Publishing options',
    '#group' => 'additional_settings',
  );

  $form['publishing_options']['status'] = array(
    '#type' => 'checkbox',
    '#title' => 'Published',
    '#default_value' => $gk_menu_section->status,
  );

  // Vertical tabs.
  $form['additional_settings'] = array(
    '#type' => 'vertical_tabs',
    '#weight' => 900,
  );

  // Actions.
  $form['actions'] = array(
    '#type' => 'actions',
    '#weight' => 1000,
  );

  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );

  return $form;
}

/**
 * AJAX callback for the menu select element on the gk_menu_section entity form.
 */
function gk_menu_section_form_ajax_callback__menu($form, $form_state) {
  return $form['menu']['ajax_wrapper'];
}

/**
 * Submit handler for the gk_menu_section entity form.
 */
function gk_menu_section_form_submit($form, &$form_state) {
  $gk_menu_section = $form_state['gk_menu_section'];

  entity_form_submit_build_entity('gk_menu_section', $gk_menu_section, $form, $form_state);
  entity_save('gk_menu_section', $gk_menu_section);

  $gk_menu_section_uri = entity_uri('gk_menu_section', $gk_menu_section);
  $form_state['redirect'] = $gk_menu_section_uri['path'];

  drupal_set_message(t('Menu section %title saved.', array(
    '%title' => entity_label('gk_menu_section', $gk_menu_section),
  )));
}

/**
 * Entity builder callback used by the gk_menu_section entity form.
 *
 * @see entity_form_submit_build_entity()
 */
function gk_menu_section_form_entity_builder($entity_type, GKMenuSection $gk_menu_section, $form, $form_state) {
  $menu_info = $form_state['values']['menu']['ajax_wrapper'];

  $gk_menu_section->mid = $menu_info['mid'];
  $gk_menu_section->parent = $menu_info['section']['msid'];
  $gk_menu_section->weight = $menu_info['section']['weight'];
}

/**
 * Entity: gk_menu_section_type ================================================
 */

/**
 * Entity form: gk_menu_section_type
 */
function gk_menu_section_type_form($form, &$form_state, $gk_menu_section_type, $op = 'edit') {
  if ($op == 'clone') {
    $gk_menu_section_type->label .= ' (cloned)';
    $gk_menu_section_type->type = '';
  }

  $form_state['gk_menu_section_type'] = $gk_menu_section_type;

  $form['label'] = array(
    '#title' => t('Label'),
    '#type' => 'textfield',
    '#default_value' => isset($gk_menu_section_type->label) ? $gk_menu_section_type->label : '',
    '#description' => t('The human-readable name of this type.'),
    '#required' => TRUE,
    '#size' => 30,
  );

  $form['type'] = array(
    '#type' => 'machine_name',
    '#default_value' => isset($gk_menu_section_type->type) ? $gk_menu_section_type->type : '',
    '#maxlength' => 32,
    '#disabled' => $gk_menu_section_type->isLocked() && $op != 'clone',
    '#machine_name' => array(
      'exists' => 'gk_menu_section_type_list',
      'source' => array('label'),
    ),
    '#description' => t('A unique machine-readable name for this type. It must only contain lowercase letters, numbers, and underscores.'),
  );

  $form['description'] = array(
    '#type' => 'textarea',
    '#title' => t('Description'),
    '#default_value' => isset($gk_menu_section_type->description) ? $gk_menu_section_type->description : '',
    '#description' => t('A description of this type.'),
  );

  // Actions.
  $form['actions'] = array(
    '#type' => 'actions',
    '#weight' => 1000,
  );

  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );

  return $form;
}

/**
 * Submit handler for the gk_menu_section_type entity form.
 */
function gk_menu_section_type_form_submit(&$form, &$form_state) {
  $gk_menu_section_type = entity_ui_form_submit_build_entity($form, $form_state);
  entity_save('gk_menu_section_type', $gk_menu_section_type);

  // Redirect the user back to the list of gk_menu_section_type entities.
  $form_state['redirect'] = 'admin/structure/gk-menus/menu-section-types';
}
