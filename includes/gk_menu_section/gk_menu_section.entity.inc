<?php

/**
 * @file
 * Entity classes for the gk_menu_section(_type) entities.
 */

/**
 * Entity class: gk_menu_section
 */
class GKMenuSection extends Entity implements GKMenusHasMenuEntityInterface {
  /**
   *
   */
  protected function defaultUri() {
    return array(
      'path' => 'admin/content/gk-menus/menu-sections/' . $this->msid,
    );
  }

  /**
   * Implements GKMenusHasMenuEntityInterface::getMenuEntity().
   */
  public function getMenuEntity() {
    if (empty($this->is_new)) {
      return gk_menu_load($this->mid);
    }
  }

  /**
   * Helper function used when building render arrays for menu sections. Stores
   * an HTML ID against a section ID so that it can be retrieved later on.
   *
   * The need for this arises from the behaviour of drupal_html_id(), whose
   * return value is always unique regardless of it's input. This ensures we
   * only call drupal_html_id() once per input value.
   *
   * @return String
   *   The result of drupal_html_id() for the title of the given section.
   */
  public static function htmlId(GKMenuSection $gk_menu_section) {
    static $storage = array();

    if (!isset($storage[$gk_menu_section->msid])) {
      $html_id = drupal_html_id($gk_menu_section->title);
      $storage[$gk_menu_section->msid] = $html_id;
    }

    return $storage[$gk_menu_section->msid];
  }
}
