<?php

/**
 * @file
 * API functions for the gk_menu_section(_type) entities.
 */

/**
 * Entity: gk_menu_section =====================================================
 */

/**
 * List of gk_menu_section entities.
 */
function gk_menu_section_list($mid = NULL) {
  $query = db_select('gk_menu_section', 'ms')->fields('ms', array('msid'));

  if (isset($mid)) {
    $query->condition('ms.mid', $mid);
  }

  if ($msids = $query->execute()->fetchCol()) {
    return gk_menu_section_load_multiple($msids);
  }
}

/**
 * Entity access callback: gk_menu_section
 */
function gk_menu_section_access($op, $gk_menu_section, $account = NULL, $entity_type = NULL) {
  if (!isset($account)) {
    $account = $GLOBALS['user'];
  }

  return user_access('administer gk menus content', $account)
    && user_access('administer gk_menu_section', $account);
}

/**
 * Entity view callback: gk_menu_section
 */
function gk_menu_section_view($gk_menu_section, $view_mode = 'full', $langcode = NULL) {
  return entity_view('gk_menu_section', array($gk_menu_section), $view_mode, $langcode, $page);
}

/**
 * Entity load callback: gk_menu_section
 */
function gk_menu_section_load($msid, $reset = FALSE) {
  $gk_menu_sections = gk_menu_section_load_multiple(array($msid), array(), $reset);
  return reset($gk_menu_sections);
}

/**
 * Entity load multiple callback: gk_menu_section
 */
function gk_menu_section_load_multiple($msids = array(), $conditions = array(), $reset = FALSE) {
  return entity_load('gk_menu_section', $msids, $conditions, $reset);
}

/**
 * Entity: gk_menu_section_type  ===============================================
 */

/**
 * List of gk_menu_section_type entities.
 */
function gk_menu_section_type_list($type_name = NULL) {
  $types = entity_load_multiple_by_name('gk_menu_section_type', isset($type_name) ? array($type_name) : FALSE);
  return isset($type_name) ? reset($types) : $types;
}

/**
 * Entity access callback (used by admin UI): gk_menu_section_type
 */
function gk_menu_section_type_access($op, $entity = NULL, $account = NULL) {
  return user_access('administer gk menus', $account);
}

/**
 * Entity load callback: gk_menu_section_type
 */
function gk_menu_section_type_load($gk_menu_section_type) {
  return gk_menu_section_type_list($gk_menu_section_type);
}
