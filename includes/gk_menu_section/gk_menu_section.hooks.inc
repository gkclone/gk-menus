<?php

/**
 * @file
 * Hook implementations for the gk_menu_section(_type) entities.
 */

/**
 * Implements hook_permission().
 */
function _gk_menu_section_permission() {
  return array(
    'administer gk_menu_section' => array(
      'title' => t('Administer GK menu section entities'),
    ),
  );
}

/**
 * Implements hook_entity_info().
 */
function _gk_menu_section_entity_info() {
  $entity_info = array(
    'gk_menu_section' => array(
      'label' => t('Menu Section'),
      'entity class' => 'GKMenuSection',
      'controller class' => 'GKMenuSectionController',
      'base table' => 'gk_menu_section',
      'fieldable' => TRUE,
      'load hook' => 'gk_menu_section_load',
      'uri callback' => 'entity_class_uri',
      'label callback' => 'entity_class_label',
      'access callback' => 'gk_menu_section_access',
      'module' => 'gk_menus',
      'entity keys' => array(
        'id' => 'msid',
        'bundle' => 'type',
        'label' => 'title',
      ),
      'bundles' => array(),
      'bundle keys' => array(
        'bundle' => 'type',
      ),
      'view modes' => array(
        'full' => array(
          'label' => t('Full'),
          'custom settings' => TRUE,
        ),
        'teaser' => array(
          'label' => t('Teaser'),
          'custom settings' => TRUE,
        ),
      ),
      'admin ui' => array(
        'path' => 'admin/content/gk-menus/menu-sections',
        'file' => 'includes/gk_menu_section/gk_menu_section.entity.admin.inc',
        'controller class' => 'GKMenuSectionAdminUIController',
      ),
    ),
    'gk_menu_section_type' => array(
      'label' => t('Menu Section Type'),
      'entity class' => 'GKEntityType',
      'controller class' => 'EntityAPIControllerExportable',
      'base table' => 'gk_menu_section_type',
      'fieldable' => FALSE,
      'exportable' => TRUE,
      'bundle of' => 'gk_menu_section',
      'access callback' => 'gk_menu_section_type_access',
      'module' => 'gk_menus',
      'entity keys' => array(
        'id' => 'id',
        'name' => 'type',
        'label' => 'label',
      ),
      'admin ui' => array(
        'path' => 'admin/structure/gk-menus/menu-section-types',
        'file' => 'includes/gk_menu_section/gk_menu_section.entity.admin.inc',
      ),
    ),
  );

  return $entity_info;
}

/**
 * Implements hook_entity_info_alter().
 */
function _gk_menu_section_entity_info_alter(&$entity_info) {
  foreach (gk_menu_section_type_list() as $type => $info) {
    $entity_info['gk_menu_section']['bundles'][$type] = array(
      'label' => $info->label,
      'admin' => array(
        'bundle argument' => 5,
        'path' => 'admin/structure/gk-menus/menu-section-types/manage/%gk_menu_section_type',
        'real path' => 'admin/structure/gk-menus/menu-section-types/manage/' . $type,
        'access arguments' => array('administer gk menus'),
      ),
    );
  }
}
