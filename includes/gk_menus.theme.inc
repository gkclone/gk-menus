<?php

/**
 * @file
 * Theming functions for the Menus module.
 */

/**
 * Preprocess variables for menu-item.tpl.php
 */
function gk_menus_preprocess_html(&$variables) {
  if ($gk_menu = menu_get_object('gk_menu')) {
    $variables['attributes_array']['class'][] = 'Page--gkMenu';
    $variables['attributes_array']['class'][] = 'Page--gkMenuTypeDefault';
  }
}

/**
 * Preprocess variables for menu-item.tpl.php
 */
function gk_menus_preprocess_entity(&$variables) {
  if ($variables['elements']['#entity_type'] == 'gk_menu_item') {
    $gk_menu_item = $variables['gk_menu_item'];

    // Attributes array.
    $variables['attributes_array']['class'] = array('Box', 'Box--gkMenuItem', 'GKMenuItem');
    $variables['attributes_array']['id'] = drupal_html_id($gk_menu_item->title);
    $variables['attributes_array']['data-miid'] = $gk_menu_item->miid;

    // Content attributes array.
    $variables['content_attributes_array']['class'] = array('Box-content');

    // Description field.
    if ($description_field_items = field_get_items('gk_menu_item', $gk_menu_item, 'field_menu_item_description')) {
      $variables['description'] = check_markup($description_field_items[0]['value']);
    }

    // Price.
    if (($gk_menu = menu_get_object('gk_menu', 1)) && $gk_menu_price_band = gk_menus_get_active_price_band($gk_menu)) {
      static $item__price_band = NULL;

      if (!isset($item__price_band)) {
        $item__price_band = GKMenuItem::getMenuItemAssociations('price_band', array(
          'mid' => $gk_menu->mid,
          'mpbid' => $gk_menu_price_band->mpbid,
        ));
      }

      if (isset($item__price_band[$gk_menu_item->miid])) {
        $variables['price'] = $item__price_band[$gk_menu_item->miid]->price;
        $variables['attributes_array']['class'][] = 'GKMenuItem--hasPrice';
      }
    }

    // Taxonomy (nutrition information and menu item attributes).
    if ($terms = $gk_menu_item->getTaxonomyTerms()) {
      $items = array();

      foreach ($terms as $tid => $term) {
        static $classes = array();

        if (!isset($classes[$tid])) {
          $classes[$tid] = drupal_html_class($term->name);

          // Ensure class names don't begin with a number.
          if (is_numeric($classes[$tid]{0})) {
            $classes[$tid] = 'n' . $classes[$tid];
          }
        }

        $items[] = array(
          'data' => $term->name,
          'class' => array($classes[$tid]),
          'id' => 'taxonomy-term-' . $tid,
        );
      }

      // Build HTML markup if there were terms.
      if (!empty($items)) {
        $variables['terms'] = array(
          '#theme' => 'item_list',
          '#items' => $items,
          '#attributes' => array(
            'class' => array('GKMenuItem-terms'),
          ),
        );
      }
    }
  }
}

/**
 * Preprocess variables for theme panels_pane.
 */
function gk_menus_preprocess_panels_pane(&$variables) {
  // Add appropriate classes to this modules panels panes.
  static $pane_classes = array(
    'gk_menus_availability' => 'Box--gkMenusAvailability',
    'current_location' => 'Box--gkMenusCurrentLocation',
    'key' => 'Box--gkMenusKey',
    'navigation' => 'GKMenusNavigation',
  );

  if (isset($pane_classes[$variables['pane']->type]) && $variables['pane']->type == $variables['pane']->subtype) {
    $variables['attributes_array']['class'][] = $pane_classes[$variables['pane']->type];
  }
}
