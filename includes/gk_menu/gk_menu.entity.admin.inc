<?php

/**
 * @file
 * Admin UI for the gk_menu(_type) entities.
 */

/**
 * Entity: gk_menu =============================================================
 */

/**
 * Entity form: gk_menu
 */
function gk_menu_form($form, &$form_state, $gk_menu) {
  $form_state['gk_menu'] = $gk_menu;
  $entity_id = entity_id('gk_menu', $gk_menu);

  $form['title'] = array(
    '#type' => 'textfield',
    '#title' => t('Name'),
    '#required' => TRUE,
    '#default_value' => $gk_menu->title,
  );

  $form['uid'] = array(
    '#type' => 'value',
    '#value' => $gk_menu->uid,
  );

  // Field API form elements.
  field_attach_form('gk_menu', $gk_menu, $form, $form_state);

  //
  // Menu meta data entity form.
  //
  $form['gk_menu_meta_data'] = array(
    '#type' => 'fieldset',
    '#title' => 'Meta data',
    '#group' => 'additional_settings',
    '#tree' => TRUE,
  );

  form_load_include($form_state, 'inc', 'gk_menus', 'includes/gk_menu_meta_data/gk_menu_meta_data.entity.admin');
  gk_menu_meta_data_embed_entity_form('gk_menu', $gk_menu, $form, $form_state);

  // Publishing options.
  $form['publishing_options'] = array(
    '#type' => 'fieldset',
    '#title' => 'Publishing options',
    '#group' => 'additional_settings',
  );

  $form['publishing_options']['status'] = array(
    '#type' => 'checkbox',
    '#title' => 'Published',
    '#default_value' => $gk_menu->status,
  );

  $form['publishing_options']['weight'] = array(
    '#type' => 'select',
    '#title' => 'Weight',
    '#default_value' => $gk_menu->weight,
    '#options' => drupal_map_assoc(range(-50, 50)),
  );

  // Vertical tabs.
  $form['additional_settings'] = array(
    '#type' => 'vertical_tabs',
    '#weight' => 900,
  );

  // Actions.
  $form['actions'] = array(
    '#type' => 'actions',
    '#weight' => 1000,
  );

  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );

  return $form;
}

/**
 * Submit handler for the gk_menu entity form.
 */
function gk_menu_form_submit($form, &$form_state) {
  $gk_menu = $form_state['gk_menu'];

  entity_form_submit_build_entity('gk_menu', $gk_menu, $form, $form_state);
  entity_save('gk_menu', $gk_menu);

  $gk_menu_uri = entity_uri('gk_menu', $gk_menu);
  $form_state['redirect'] = $gk_menu_uri['path'];

  drupal_set_message(t('Menu %title saved.', array(
    '%title' => entity_label('gk_menu', $gk_menu),
  )));
}

/**
 * Entity: gk_menu_type ========================================================
 */

/**
 * Entity form: gk_menu_type
 */
function gk_menu_type_form($form, &$form_state, $gk_menu_type, $op = 'edit') {
  if ($op == 'clone') {
    $gk_menu_type->label .= ' (cloned)';
    $gk_menu_type->type = '';
  }

  $form_state['gk_menu_type'] = $gk_menu_type;

  $form['label'] = array(
    '#title' => t('Label'),
    '#type' => 'textfield',
    '#default_value' => isset($gk_menu_type->label) ? $gk_menu_type->label : '',
    '#description' => t('The human-readable name of this type.'),
    '#required' => TRUE,
    '#size' => 30,
  );

  $form['type'] = array(
    '#type' => 'machine_name',
    '#default_value' => isset($gk_menu_type->type) ? $gk_menu_type->type : '',
    '#maxlength' => 32,
    '#disabled' => $gk_menu_type->isLocked() && $op != 'clone',
    '#machine_name' => array(
      'exists' => 'gk_menu_type_list',
      'source' => array('label'),
    ),
    '#description' => t('A unique machine-readable name for this type. It must only contain lowercase letters, numbers, and underscores.'),
  );

  $form['description'] = array(
    '#type' => 'textarea',
    '#title' => t('Description'),
    '#default_value' => isset($gk_menu_type->description) ? $gk_menu_type->description : '',
    '#description' => t('A description of this type.'),
  );

  // Actions.
  $form['actions'] = array(
    '#type' => 'actions',
    '#weight' => 1000,
  );

  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );

  return $form;
}

/**
 * Submit handler for the gk_menu_type entity form.
 */
function gk_menu_type_form_submit(&$form, &$form_state) {
  $gk_menu_type = entity_ui_form_submit_build_entity($form, $form_state);
  entity_save('gk_menu_type', $gk_menu_type);

  // Redirect the user back to the list of gk_menu_type entities.
  $form_state['redirect'] = 'admin/structure/gk-menus/menu-types';
}
