<?php

/**
 * @file
 * Hook implementations for the gk_menu(_type) entities.
 */

/**
 * Implements hook_permission().
 */
function _gk_menu_permission() {
  return array(
    'administer gk_menu' => array(
      'title' => t('Administer GK menu entities'),
    ),
  );
}

/**
 * Implements hook_menu().
 */
function _gk_menu_menu() {
  $items['gk-menu/%gk_menu'] = array(
    'title callback' => 'entity_class_label',
    'title arguments' => array(1),
    'page callback' => 'gk_menu_view_page',
    'page arguments' => array(1),
    'access callback' => 'entity_access',
    'access arguments' => array('view', 'gk_menu', 1),
  );
  $items['gk-menu/%gk_menu/view'] = array(
    'title' => 'View',
    'type' => MENU_DEFAULT_LOCAL_TASK,
    'weight' => -10,
  );

  // Make it easier to edit a menu when viewing it on the front-end.
  $items['gk-menu/%/edit'] = array(
    'title' => 'Edit',
    'page callback' => '_gk_menu_redirect_to_edit_form',
    'page arguments' => array(1),
    'access callback' => 'entity_access',
    'access arguments' => array('update', 'gk_menu', 1),
    'type' => MENU_LOCAL_TASK,
    'weight' => -5,
  );

  $items['gk-menu/%gk_menu/pdf'] = array(
    'page callback' => 'gk_menu_pdf_page',
    'page arguments' => array(1),
    'access callback' => 'entity_access',
    'access arguments' => array('view', 'gk_menu', 1),
    'type' => MENU_CALLBACK,
  );

  return $items;
}

/**
 * Implements hook_entity_info().
 */
function _gk_menu_entity_info() {
  $entity_info = array(
    'gk_menu' => array(
      'label' => t('Menu'),
      'entity class' => 'GKMenu',
      'controller class' => 'GKMenuController',
      'base table' => 'gk_menu',
      'fieldable' => TRUE,
      'load hook' => 'gk_menu_load',
      'uri callback' => 'entity_class_uri',
      'label callback' => 'entity_class_label',
      'access callback' => 'gk_menu_access',
      'module' => 'gk_menus',
      'entity keys' => array(
        'id' => 'mid',
        'bundle' => 'type',
        'label' => 'title',
      ),
      'bundles' => array(),
      'bundle keys' => array(
        'bundle' => 'type',
      ),
      'view modes' => array(
        'full' => array(
          'label' => t('Full'),
          'custom settings' => TRUE,
        ),
        'teaser' => array(
          'label' => t('Teaser'),
          'custom settings' => TRUE,
        ),
      ),
      'admin ui' => array(
        'path' => 'admin/content/gk-menus/menus',
        'file' => 'includes/gk_menu/gk_menu.entity.admin.inc',
        'controller class' => 'GKMenuAdminUIController',
      ),
    ),
    'gk_menu_type' => array(
      'label' => t('Menu Type'),
      'entity class' => 'GKEntityType',
      'controller class' => 'EntityAPIControllerExportable',
      'base table' => 'gk_menu_type',
      'fieldable' => FALSE,
      'exportable' => TRUE,
      'bundle of' => 'gk_menu',
      'access callback' => 'gk_menu_type_access',
      'module' => 'gk_menus',
      'entity keys' => array(
        'id' => 'id',
        'name' => 'type',
        'label' => 'label',
      ),
      'admin ui' => array(
        'path' => 'admin/structure/gk-menus/menu-types',
        'file' => 'includes/gk_menu/gk_menu.entity.admin.inc',
      ),
    ),
  );

  return $entity_info;
}

/**
 * Implements hook_entity_info_alter().
 */
function _gk_menu_entity_info_alter(&$entity_info) {
  foreach (gk_menu_type_list() as $type => $info) {
    $entity_info['gk_menu']['bundles'][$type] = array(
      'label' => $info->label,
      'admin' => array(
        'bundle argument' => 5,
        'path' => 'admin/structure/gk-menus/menu-types/manage/%gk_menu_type',
        'real path' => 'admin/structure/gk-menus/menu-types/manage/' . $type,
        'access arguments' => array('administer gk menus'),
      ),
    );
  }
}

/**
 * Implements hook_token_info().
 */
function _gk_menu_token_info() {
  return array(
    'types' => array(
      'gk-menu' => array(
        'name' => t('GK Menu'),
        'description' => t('Tokens for GK menus.'),
      ),
    ),
    'tokens' => array(
      'gk-menu' => array(
        'title' => array(
          'name' => t('GK Menu title'),
          'description' => t('The title of the GK menu.'),
        ),
      ),
    ),
  );
}

/**
 * Implements hook_tokens().
 */
function _gk_menu_tokens($type, $tokens, array $data = array(), array $options = array()) {
  $replacements = array();

  if ($type == 'gk-menu') {
    foreach ($tokens as $name => $original) {
      if ($name == 'title' && isset($data['gk_menu'])) {
        $replacements[$original] = $data['gk_menu']->title;
      }
    }
  }

  return $replacements;
}

/**
 * Implements hook_admin_paths().
 */
function _gk_menu_admin_paths() {
  return array(
    'gk-menu/*/edit' => TRUE,
  );
}

/**
 * Implements hook_pathauto_entity_alias_settings_alter().
 */
function _gk_menu_pathauto_entity_alias_settings_alter(&$entity_forms) {
  $entity_forms += array(
    'gk_menu' => 'gk_menu_edit_gk_menu_type__default_form',
  );
}

/**
 * Page callback for /gk-menu/%gk_menu.
 */
function gk_menu_view_page(GKMenu $gk_menu) {
  if (!$gk_menu->status && !user_access('administer gk menus content')) {
    return MENU_NOT_FOUND;
  }

  return gk_menu_view($gk_menu);
}

/**
 *
 */
function _gk_menu_redirect_to_edit_form($mid) {
  drupal_goto('admin/content/gk-menus/menus/' . $mid . '/edit');
}

/**
 * Page callback for /gk-menu/%gk_menu/pdf.
 */
function gk_menu_pdf_page(GKMenu $gk_menu) {
  $meta_data = gk_menus_get_active_meta_data($gk_menu);

  if (empty($meta_data['field_menu_pdf'])) {
    return MENU_NOT_FOUND;
  }

  $file = (object) $meta_data['field_menu_pdf'];
  $filename = transliteration_clean_filename($gk_menu->title);

  $headers = file_get_content_headers($file) + array(
    'Content-Disposition' => 'attachment; filename="' . $filename . '.pdf"',
  );

  file_transfer($file->uri, $headers);
}
