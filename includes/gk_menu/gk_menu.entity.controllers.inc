<?php

/**
 * @file
 * Controller classes for the gk_menu(_type) entities.
 */

/**
 * Entity controller class: gk_menu
 */
class GKMenuController extends GKEntityBundleableController {
  public function create(array $values = array()) {
    $values += array(
      'mmdid' => NULL,
      'weight' => 0,
    );

    return parent::create($values);
  }

  /**
   * Overridden to clear the content builder cache for this menu.
   *
   * @see EntityAPIController::save()
   */
  public function save($gk_menu, DatabaseTransaction $transaction = NULL) {
    if ($return = parent::save($gk_menu, $transaction)) {
      GKMenu::flushCaches($gk_menu->mid);
    }

    return $return;
  }

  /**
   * Overridden to delete associated gk_menu_meta_data entities.
   *
   * @see EntityAPIController::delete()
   */
  public function delete($ids, DatabaseTransaction $transaction = NULL) {
    $gk_menus = $ids ? $this->load($ids) : FALSE;

    if ($gk_menus) {
      $mmdids = array();

      foreach ($gk_menus as $gk_menu) {
        $mmdids[] = $gk_menu->mmdid;
      }

      entity_get_controller('gk_menu_meta_data')->delete($mmdids);
    }

    parent::delete($ids, $transaction);
  }
}

/**
 * Admin UI controller class for gk_menu entities.
 */
class GKMenuAdminUIController extends GKEntityBundleableUIController {
  protected function overviewTableHeaders() {
    $headers = parent::overviewTableHeaders();

    // Add a column for the weight of this menu.
    $headers['weight'] = array(
      'label' => 'weight',
      'weight' => 5,
    );

    return $headers;
  }

  protected function overviewGetQuery($conditions = array()) {
    $query = parent::overviewGetQuery($conditions);

    // Order the results by weight rather than primary key.
    $query->order = array();
    $query->propertyOrderBy('weight');

    return $query;
  }
}
