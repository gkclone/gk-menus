<?php

/**
 * @file
 * API functions for the gk_menu(_type) entities.
 */

/**
 * Entity: gk_menu =============================================================
 */

/**
 * List of gk_menu entities.
 */
function gk_menu_list() {
  $query = db_select('gk_menu', 'm')->fields('m', array('mid'))->orderBy('m.weight');

  if ($mids = $query->execute()->fetchCol()) {
    return gk_menu_load_multiple($mids);
  }
}

/**
 * Entity access callback: gk_menu
 */
function gk_menu_access($op, $gk_menu, $account = NULL, $entity_type = NULL) {
  if (!isset($account)) {
    $account = $GLOBALS['user'];
  }

  static $administer_gk_menus = array(
    'create' => 1, 'update' => 1, 'edit' => 1, 'delete' => 1,
  );
  if (isset($administer_gk_menus[$op])) {
    return user_access('administer gk menus content', $account)
      && user_access('administer gk_menu', $account);
  }

  if ($op == 'view') {
    return user_access('access content', $account);
  }
}

/**
 * Entity view callback: gk_menu
 */
function gk_menu_view($gk_menu, $view_mode = 'full', $langcode = NULL, $page = NULL) {
  return entity_view('gk_menu', array($gk_menu), $view_mode, $langcode, $page);
}

/**
 * Entity load callback: gk_menu
 */
function gk_menu_load($mid, $reset = FALSE) {
  $gk_menus = gk_menu_load_multiple(array($mid), array(), $reset);
  return reset($gk_menus);
}

/**
 * Entity load multiple callback: gk_menu
 */
function gk_menu_load_multiple($mids = array(), $conditions = array(), $reset = FALSE) {
  return entity_load('gk_menu', $mids, $conditions, $reset);
}

/**
 * Entity: gk_menu_type ========================================================
 */

/**
 * List of gk_menu_type entities.
 */
function gk_menu_type_list($type_name = NULL) {
  $types = entity_load_multiple_by_name('gk_menu_type', isset($type_name) ? array($type_name) : FALSE);
  return isset($type_name) ? reset($types) : $types;
}

/**
 * Entity access callback (used by admin UI): gk_menu_type
 */
function gk_menu_type_access($op, $entity = NULL, $account = NULL) {
  return user_access('administer gk menus', $account);
}

/**
 * Entity load callback: gk_menu_type
 */
function gk_menu_type_load($gk_menu_type) {
  return gk_menu_type_list($gk_menu_type);
}
