<?php

/**
 * @file
 * Entity classes for the gk_menu(_type) entities.
 */

/**
 * Entity class: gk_menu
 */
class GKMenu extends Entity implements GKMenusHasMetaDataEntityInterface {
  private $hierarchy;
  private $gk_menu_items;

  /**
   *
   */
  protected function defaultUri() {
    return array(
      'path' => 'gk-menu/' . (!empty($this->mid) ? $this->mid : '%gk_menu'),
    );
  }

  /**
   * Implements GKMenusHasMetaDataEntityInterface::getMetaDataEntity().
   */
  public function getMetaDataEntity() {
    if (!empty($this->mmdid)) {
      return gk_menu_meta_data_load($this->mmdid);
    }
  }

  /**
   * Implements GKMenusHasMetaDataEntityInterface::getMetaDataParentEntity().
   */
  public function getMetaDataParentEntity() {
    // Menus do not have a meta data parent entity.
    return NULL;
  }

  /**
   * Implements GKMenusHasMetaDataEntityInterface::getMetaData().
   */
  public function getMetaData() {
    if ($gk_menu_meta_data = $this->getMetaDataEntity()) {
      return GKMenuMetaData::getMetaData($gk_menu_meta_data, $this);
    }
  }

  /**
   * Get a cache value.
   *
   * @see GKMenu::getCacheId()
   */
  protected function getCache($type, $mpbid = NULL) {
    $return = NULL;

    if (self::isCacheEnabled()) {
      // Display useful debug information to administrators?
      $cache_debug = variable_get('gk_menus_cache_debug', FALSE) &&
        user_access('administer gk menus');

      $cache_id = $this->getCacheId($type, $mpbid);

      if ($cache = cache_get($cache_id)) {
        $return = $cache->data;

        if ($cache_debug) {
          drupal_set_message("Menu $type cache HIT for cid: $cache_id");
        }
      }
      elseif ($cache_debug) {
        drupal_set_message("Menu $type cache MISS for cid: $cache_id");
      }
    }
    // Let an administrator know if the cache is not enabled.
    elseif (user_access('administer gk menus')) {
      drupal_set_message(t('Menu cache is not enabled. !link', array(
        '!link' => l('Enable it?', 'admin/config/gk-menus'),
      )), 'warning', FALSE);
    }

    return $return;
  }

  /**
   * Set a cache value.
   *
   * @param $data Array
   *   The value to store in the cache.
   *
   * @see GKMenu::getCacheId()
   */
  protected function setCache($type, $data, $mpbid = NULL) {
    if (self::isCacheEnabled()) {
      cache_set($this->getCacheId($type, $mpbid), $data);
    }
  }

  /**
   * Helper function used to build a cache ID.
   *
   * @param $type String
   *   Type of cache. Either 'content' or 'hierarchy'.
   * @param $mpbid Integer
   *   Optional menu price band ID.
   *
   * @return String
   */
  protected function getCacheId($type, $mpbid = NULL) {
    $cache_id = 'gk_menus:' . $type . ':mid:' . $this->mid;

    if (isset($mpbid)) {
      $cache_id .= ':mpbid:' . $mpbid;
    }

    return $cache_id;
  }

  /**
   * Helper function used to flush menu caches.
   *
   * @param $mid Integer
   *   A menu ID.
   * @param $types Array
   *   Optional. Zero, one or all of ['content', 'hierarchy']. If an empty value
   *   is provided then all cache types are flushed.
   */
  public static function flushCaches($mid, $types = NULL) {
    if (empty($types)) {
      $types = array('hierarchy', 'content');
    }
    elseif (is_string($types)) {
      $types = array($types);
    }

    foreach ($types as $type) {
      cache_clear_all('gk_menus:' . $type . ':mid:' . $mid, 'cache', TRUE);
    }
  }

  /**
   * Helper function for determining whether menu caching is enabled.
   *
   * @return Boolean
   */
  public static function isCacheEnabled() {
    return variable_get('gk_menus_cache_enabled', TRUE);
  }

  /**
   * Get a list of gk_menu_item entities associated with this menu.
   *
   * @param $mpbid Integer
   *   Optional menu price band ID.
   *
   * @return Array
   */
  public function getMenuItems($mpbid = NULL) {
    if (!isset($this->gk_menu_items)) {
      // Get associations for items that appear on this menu regardless of price
      // bands.
      $items__sections = GKMenuItem::getMenuItemAssociations('section', array(
        'mid' => $this->mid,
      ));

      // If a price band ID was passed in then get a list of associations that
      // respect this and filter out items from $items__sections.
      if (isset($mpbid)) {
        $items__price_bands = GKMenuItem::getMenuItemAssociations('price_band', array(
          'mid' => $this->mid,
          'mpbid' => $mpbid,
        ));

        if ($items__price_bands) {
          $items__sections = array_intersect_key($items__sections, $items__price_bands);
        }
      }

      $this->gk_menu_items = gk_menu_item_load_multiple(array_keys($items__sections), array(
        'status' => 1,
      ));
    }

    return $this->gk_menu_items;
  }

  /**
   * Get this menu in nested hierarchical form.
   *
   * @see GKMenu::buildHierarchy()
   */
  public function getHierarchy($mpbid = NULL) {
    if (!isset($this->hierarchy)) {
      if ($cache = $this->getCache('hierarchy', $mpbid)) {
        $this->hierarchy = $cache;
      }
      else {
        $this->hierarchy = $this->buildHierarchy($mpbid);
        $this->setCache('hierarchy', $this->hierarchy, $mpbid);
      }
    }

    return $this->hierarchy;
  }

  /**
   * Build this menu in nested hierarchical form.
   *
   * @param $mpbid Integer
   *   Optional price band ID. If provided, menu items that don't have a price
   *   band association will not be included. Consequently, sections that are
   *   left empty because of this will not be included either.
   *
   * @return Array
   *   An array to represent this menu including sections and menu items.
   *
   * Example:
   *
   * array(
   *   1 (msid) => array(
   *     'section' => "Starters" (GKMenuSection),
   *     'items' => array(
   *       1 (miid) => "Olives" (GKMenuItem),
   *       2 (miid) => "Bread" (GKMenuItem),
   *     ),
   *   ),
   *   2 (msid) => array(
   *     'section' => "Mains" (GKMenuSection),
   *     'items' => array(),
   *     3 (msid) => array(
   *       'section' => "Meat" (GKMenuSection),
   *       'items' => array(),
   *       4 (msid) => array(
   *         'section' => "Grill" (GKMenuSection),
   *         'items' => array(
   *           3 (miid) => "Steak" (GKMenuItem),
   *         ),
   *       ),
   *       5 (msid) => array(
   *         'section' => "Burgers" (GKMenuSection),
   *         'items' => array(
   *           4 (miid) => "Hamburger" (GKMenuItem),
   *           5 (miid) => "Cheeseburger" (GKMenuItem),
   *         ),
   *       )
   *     ),
   *   )
   * )
   */
  private function buildHierarchy($mpbid = NULL) {
    $hierarchy = array();

    // Load the gk_menu_item entities associated with this menu.
    if ($gk_menu_items = $this->getMenuItems($mpbid)) {
      // Load the gk_menu_section entities associated with this menu.
      $msids = db_select('gk_menu_section', 's')
        ->fields('s', array('msid'))
        ->condition('s.mid', $this->mid)
        ->condition('s.status', 1)
        ->execute()
        ->fetchCol();

      $gk_menu_sections = gk_menu_section_load_multiple($msids);

      // Build an array for each section that contains one or more menu items as
      // direct children. Sections that fall outside of this will be accounted
      // for later.
      $items__sections = GKMenuItem::getMenuItemAssociations('section', array(
        'mid' => $this->mid,
      ));

      $sections_with_items = array();

      foreach ($gk_menu_items as $miid => $gk_menu_item) {
        $msid = $items__sections[$miid]->msid;

        // If the section this item belongs to wasn't loaded then move on. This
        // would happen if a section entity was unpublished and the items it
        // contains were left published.
        if (!isset($gk_menu_sections[$msid])) {
          continue;
        }

        if (!isset($sections_with_items[$msid]['section'])) {
          $sections_with_items[$msid]['section'] = $gk_menu_sections[$msid];
        }

        $sections_with_items[$msid]['items'][$miid] = $gk_menu_item;
      }

      // Sort menu items by weight within their sections.
      foreach ($sections_with_items as $msid => $section) {
        $items = &$sections_with_items[$msid]['items'];

        uasort($items, function ($a, $b) use ($items__sections) {
          $weight_a = $items__sections[$a->miid]->weight;
          $weight_b = $items__sections[$b->miid]->weight;

          if ($weight_a == $weight_b) {
            return 0;
          }

          return $weight_a > $weight_b ? 1 : -1;
        });
      }

      // Determine the depth and path of all sections in the menu. Sections at
      // the root of the menu have depth 0 and a single item in their path. A
      // path is made up of msid's and will be used as keys in the hierarchy.
      $sections_by_depth = $parents_by_depth = array();

      foreach ($gk_menu_sections as $gk_menu_section) {
        $depth = 0;
        $msid = $gk_menu_section->msid;
        $path_to_section = array($msid);
        $temp__gk_menu_section = $gk_menu_section;

        while ($temp__gk_menu_section->parent) {
          $parent = $temp__gk_menu_section->parent;

          // If the parent property is a valid msid then increment $depth and
          // update the path. Keep going until we hit the root of the menu.
          if (isset($gk_menu_sections[$parent]) && ++$depth) {
            $temp__gk_menu_section = $gk_menu_sections[$parent];
            array_unshift($path_to_section, $parent);
          }
          // If for some reason the parent property is invalid then bail out.
          else {
            break;
          }
        }

        // If this section was discovered because one or more menu items belong
        // to it then we already have an array that represents the section.
        if (isset($sections_with_items[$msid])) {
          $section = $sections_with_items[$msid];
        }
        // Else, this section doesn't have any menu items as direct children and
        // we haven't already built an array to represent it in the hierarchy.
        else {
          $section = array(
            'section' => $gk_menu_section,
            'items' => array(),
          );
        }

        $sections_by_depth[$depth][$msid] = $section + array(
          'path' => $path_to_section,
        );

        // Keep track of the parent IDs so that we can easily lookup the parents
        // for any given section later on.
        if ($depth > 0) {
          $parents_by_depth[$depth][$gk_menu_section->parent] = 1;
        }
      }

      // Ensure $sections_by_depth is in depth ascending order.
      ksort($sections_by_depth);

      // Finally, place each section into the hierarchy array.
      $max_depth = count($sections_by_depth) - 1;

      foreach ($sections_by_depth as $depth => $sections) {
        // Sort sections by weight.
        uasort($sections, function ($a, $b) {
          if ($a['section']->weight == $b['section']->weight) {
            return 0;
          }

          return $a['section']->weight > $b['section']->weight ? 1 : -1;
        });

        foreach ($sections as $msid => $section) {
          // If the section has no items then we might not want to include it.
          if (empty($section['items'])) {
            // If the section is at the max depth in the tree then definitely
            // don't include it since it doesn't even have child sections. If
            // the section is somewhere else in the tree then it could have
            // child sections that contain items, so we'll check the parents of
            // sections in the next depth of the tree.
            if ($depth == $max_depth || !isset($parents_by_depth[$depth + 1][$msid])) {
              continue;
            }
          }

          // We don't actually want the path to be present in the hierarchy.
          $path = $section['path'];
          unset($section['path']);

          drupal_array_set_nested_value($hierarchy, $path, $section);
        }
      }
    }

    return $hierarchy;
  }

  /**
   * Build a Drupal render array to represent this menu.
   */
  public function buildContent($view_mode = 'full', $langcode = NULL) {
    $build = array();

    // Determine a price band ID to filter items on (there may not be one, in
    // which case we'll just display the sample menu containing all items). We
    // don't cache this part of the build because it's contextual.
    $mpbid = NULL;

    if ($gk_menu_price_band = gk_menus_get_active_price_band($this)) {
      $mpbid = $gk_menu_price_band->mpbid;
    }

    // It's expensive to build the menu so load a cached version if one exists.
    if ($cache = $this->getCache('content', $mpbid)) {
      $build = $cache;
    }
    elseif ($hierarchy = $this->getHierarchy($mpbid)) {
      $build = self::buildSections($hierarchy);
      drupal_alter('gk_menus_build', $build, $this);
      $this->setCache('content', $build, $mpbid);
    }

    // Determine a meta data entity from which we can pull a description for
    // this menu. We don't cache this part of the build because it's contextual.
    if ($meta_data = gk_menus_get_active_meta_data($this)) {
      if (!empty($meta_data['field_menu_description'])) {
        $build['description'] = array(
          '#theme_wrappers' => array('container'),
          '#attributes' => array(
            'class' => array('Box-header'),
          ),
          '#markup' => check_markup($meta_data['field_menu_description']['value']),
          '#weight' => -10,
        );
      }
    }

    return $build;
  }

  /**
   * Helper function used to build a Drupal render array for one depth of the
   * hierarchy.
   */
  private static function buildSections($sections, $depth = 0) {
    $build = array();

    foreach ($sections as $msid => $section) {
      $gk_menu_section = $section['section'];

      $section_build = array(
        '#weight' => $gk_menu_section->weight,
        '#theme_wrappers' => array('container'),
        '#attributes' => array(
          'id' => GKMenuSection::htmlId($gk_menu_section),
          'class' => array('Box', 'Box--menuSection'),
        ),
        'inner' => array(
          '#theme_wrappers' => array('container'),
          '#attributes' => array(
            'class' => array('Box-inner'),
          ),
          'title' => array(
            '#weight' => -100,
            '#theme' => 'html_tag',
            '#tag' => 'h' . ($depth + 2),
            '#attributes' => array(
              'class' => array('Box-title'),
            ),
            '#value' => t($gk_menu_section->title),
          ),
        ),
      );

      // Highlighted section?
      if (($highlighted = field_get_items('gk_menu_section', $gk_menu_section, 'field_menu_section_highlighted')) && $highlighted[0]['value']) {
        $section_build['#attributes']['class'][] = 'Box--highlighted';
      }

      // Section description.
      if ($description = field_get_items('gk_menu_section', $gk_menu_section, 'field_menu_section_description')) {
        $section_build['inner']['description'] = array(
          '#weight' => -90,
          '#theme_wrappers' => array('container'),
          '#attributes' => array(
            'class' => array('Box-header'),
          ),
          '#markup' => check_markup($description[0]['value'], $description[0]['format']),
        );
      }

      // Actual menu items.
      if (!empty($section['items'])) {
        $section_build['inner']['items'] = array(
          '#weight' => -80,
          '#theme_wrappers' => array('container'),
          '#attributes' => array(
            'class' => array('Box-content'),
          ),
        );

        $item_weight = 0;

        foreach ($section['items'] as $miid => $gk_menu_item) {
          $item_build = gk_menu_item_view($gk_menu_item);
          $item_build['gk_menu_item'][$miid]['#weight'] = $item_weight++;
          $section_build['inner']['items'] += $item_build['gk_menu_item'];
        }
      }

      // Process any child sections in the same way until there is nothing left.
      if ($child_sections = array_diff_key($section, array('section' => 1, 'items' => 1))) {
        $section_build['inner'] += self::buildSections($child_sections, $depth + 1);
      }

      // Section footer.
      if ($footer = field_get_items('gk_menu_section', $gk_menu_section, 'field_menu_section_footer')) {
        $section_build['inner']['footer'] = array(
          '#weight' => 100,
          '#theme_wrappers' => array('container'),
          '#attributes' => array(
            'class' => array('Box-footer'),
          ),
          '#markup' => check_markup($footer[0]['value'], $footer[0]['format']),
        );
      }

      $build[$msid] = $section_build;
    }

    // Make it possible to alter "layers" of the menu at a time. This is better
    // than using hook_gk_menus_build if you need to change something about all
    // sections in the menu, because you won't have to "discover" each section
    // recursively or otherwise.
    drupal_alter('gk_menus_build_sections', $build, $depth);

    return $build;
  }
}
