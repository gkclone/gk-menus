<?php
/**
 * @file
 * gk_menus.features.user_role.inc
 */

/**
 * Implements hook_user_default_roles().
 */
function gk_menus_user_default_roles() {
  $roles = array();

  // Exported role: gk menus content administrator.
  $roles['gk menus content administrator'] = array(
    'name' => 'gk menus content administrator',
    'weight' => 5,
  );

  return $roles;
}
