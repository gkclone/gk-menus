<?php
/**
 * @file
 * gk_menus.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function gk_menus_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'administer gk_menu'.
  $permissions['administer gk_menu'] = array(
    'name' => 'administer gk_menu',
    'roles' => array(
      'gk menus content administrator' => 'gk menus content administrator',
    ),
    'module' => 'gk_menus',
  );

  // Exported permission: 'administer gk_menu_item'.
  $permissions['administer gk_menu_item'] = array(
    'name' => 'administer gk_menu_item',
    'roles' => array(
      'gk menus content administrator' => 'gk menus content administrator',
    ),
    'module' => 'gk_menus',
  );

  // Exported permission: 'administer gk_menu_meta_data'.
  $permissions['administer gk_menu_meta_data'] = array(
    'name' => 'administer gk_menu_meta_data',
    'roles' => array(),
    'module' => 'gk_menus',
  );

  // Exported permission: 'administer gk_menu_price_band'.
  $permissions['administer gk_menu_price_band'] = array(
    'name' => 'administer gk_menu_price_band',
    'roles' => array(
      'gk menus content administrator' => 'gk menus content administrator',
    ),
    'module' => 'gk_menus',
  );

  // Exported permission: 'administer gk_menu_section'.
  $permissions['administer gk_menu_section'] = array(
    'name' => 'administer gk_menu_section',
    'roles' => array(
      'gk menus content administrator' => 'gk menus content administrator',
    ),
    'module' => 'gk_menus',
  );

  return $permissions;
}
