<?php
/**
 * @file
 * gk_menus.features.taxonomy.inc
 */

/**
 * Implements hook_taxonomy_default_vocabularies().
 */
function gk_menus_taxonomy_default_vocabularies() {
  return array(
    'menu_item_attributes' => array(
      'name' => 'Menu item attributes',
      'machine_name' => 'menu_item_attributes',
      'description' => '',
      'hierarchy' => 0,
      'module' => 'taxonomy',
      'weight' => 0,
    ),
    'nutrition_information' => array(
      'name' => 'Nutrition information',
      'machine_name' => 'nutrition_information',
      'description' => '',
      'hierarchy' => 0,
      'module' => 'taxonomy',
      'weight' => 0,
    ),
  );
}
