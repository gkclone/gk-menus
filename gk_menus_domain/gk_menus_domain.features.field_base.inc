<?php
/**
 * @file
 * gk_menus_domain.features.field_base.inc
 */

/**
 * Implements hook_field_default_field_bases().
 */
function gk_menus_domain_field_default_field_bases() {
  $field_bases = array();

  // Exported field_base: 'domain_610f277c7c5c0fb0e77ab738d'
  $field_bases['domain_610f277c7c5c0fb0e77ab738d'] = array(
    'active' => 1,
    'cardinality' => -1,
    'deleted' => 0,
    'entity_types' => array(
      0 => 'gk_menu_meta_data',
    ),
    'field_name' => 'domain_610f277c7c5c0fb0e77ab738d',
    'foreign keys' => array(),
    'indexes' => array(
      'domain_id' => array(
        0 => 'domain_id',
      ),
    ),
    'locked' => 0,
    'module' => 'domain_entity',
    'settings' => array(
      'entity_translation_sync' => FALSE,
    ),
    'translatable' => 0,
    'type' => 'domain_entity',
  );

  // Exported field_base: 'domain_67eb34df9f870f8321ef9051a'
  $field_bases['domain_67eb34df9f870f8321ef9051a'] = array(
    'active' => 1,
    'cardinality' => -1,
    'deleted' => 0,
    'entity_types' => array(
      0 => 'gk_menu_price_band',
    ),
    'field_name' => 'domain_67eb34df9f870f8321ef9051a',
    'foreign keys' => array(),
    'indexes' => array(
      'domain_id' => array(
        0 => 'domain_id',
      ),
    ),
    'locked' => 0,
    'module' => 'domain_entity',
    'settings' => array(
      'entity_translation_sync' => FALSE,
    ),
    'translatable' => 0,
    'type' => 'domain_entity',
  );

  // Exported field_base: 'domain_8edf712b4eefc7854ce604d0c'
  $field_bases['domain_8edf712b4eefc7854ce604d0c'] = array(
    'active' => 1,
    'cardinality' => -1,
    'deleted' => 0,
    'entity_types' => array(
      0 => 'gk_menu_section',
    ),
    'field_name' => 'domain_8edf712b4eefc7854ce604d0c',
    'foreign keys' => array(),
    'indexes' => array(
      'domain_id' => array(
        0 => 'domain_id',
      ),
    ),
    'locked' => 0,
    'module' => 'domain_entity',
    'settings' => array(
      'entity_translation_sync' => FALSE,
    ),
    'translatable' => 0,
    'type' => 'domain_entity',
  );

  // Exported field_base: 'domain_gk_menu'
  $field_bases['domain_gk_menu'] = array(
    'active' => 1,
    'cardinality' => -1,
    'deleted' => 0,
    'entity_types' => array(
      0 => 'gk_menu',
    ),
    'field_name' => 'domain_gk_menu',
    'foreign keys' => array(),
    'indexes' => array(
      'domain_id' => array(
        0 => 'domain_id',
      ),
    ),
    'locked' => 0,
    'module' => 'domain_entity',
    'settings' => array(
      'entity_translation_sync' => FALSE,
    ),
    'translatable' => 0,
    'type' => 'domain_entity',
  );

  // Exported field_base: 'domain_gk_menu_item'
  $field_bases['domain_gk_menu_item'] = array(
    'active' => 1,
    'cardinality' => -1,
    'deleted' => 0,
    'entity_types' => array(
      0 => 'gk_menu_item',
    ),
    'field_name' => 'domain_gk_menu_item',
    'foreign keys' => array(),
    'indexes' => array(
      'domain_id' => array(
        0 => 'domain_id',
      ),
    ),
    'locked' => 0,
    'module' => 'domain_entity',
    'settings' => array(
      'entity_translation_sync' => FALSE,
    ),
    'translatable' => 0,
    'type' => 'domain_entity',
  );

  return $field_bases;
}
