<?php
/**
 * @file
 * gk_menus_domain.features.field_instance.inc
 */

/**
 * Implements hook_field_default_field_instances().
 */
function gk_menus_domain_field_default_field_instances() {
  $field_instances = array();

  // Exported field_instance: 'gk_menu-gk_menu_type__default-domain_gk_menu'
  $field_instances['gk_menu-gk_menu_type__default-domain_gk_menu'] = array(
    'bundle' => 'gk_menu_type__default',
    'default_value' => array(),
    'deleted' => 0,
    'description' => NULL,
    'display' => array(
      'default' => array(
        'label' => 'hidden',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
      'diff_standard' => array(
        'label' => 'hidden',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
      'full' => array(
        'label' => 'hidden',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
      'node_teaser' => array(
        'label' => 'hidden',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
      'teaser' => array(
        'label' => 'hidden',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
      'token' => array(
        'label' => 'hidden',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'gk_menu',
    'field_name' => 'domain_gk_menu',
    'label' => 'Domain',
    'required' => TRUE,
    'settings' => array(
      'entity_translation_sync' => FALSE,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'module' => 'domain_entity',
      'settings' => array(),
      'type' => 'domain_entity_auto_hidden',
      'weight' => 0,
    ),
  );

  // Exported field_instance: 'gk_menu_item-gk_menu_item_type__default-domain_gk_menu_item'
  $field_instances['gk_menu_item-gk_menu_item_type__default-domain_gk_menu_item'] = array(
    'bundle' => 'gk_menu_item_type__default',
    'default_value' => array(),
    'deleted' => 0,
    'description' => NULL,
    'display' => array(
      'default' => array(
        'label' => 'hidden',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
      'diff_standard' => array(
        'label' => 'hidden',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
      'full' => array(
        'label' => 'hidden',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
      'node_teaser' => array(
        'label' => 'hidden',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
      'teaser' => array(
        'label' => 'hidden',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
      'token' => array(
        'label' => 'hidden',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'gk_menu_item',
    'field_name' => 'domain_gk_menu_item',
    'label' => 'Domain',
    'required' => TRUE,
    'settings' => array(
      'entity_translation_sync' => FALSE,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'module' => 'domain_entity',
      'settings' => array(),
      'type' => 'domain_entity_auto_hidden',
      'weight' => 4,
    ),
  );

  // Exported field_instance: 'gk_menu_meta_data-gk_menu_meta_data_type__default-domain_610f277c7c5c0fb0e77ab738d'
  $field_instances['gk_menu_meta_data-gk_menu_meta_data_type__default-domain_610f277c7c5c0fb0e77ab738d'] = array(
    'bundle' => 'gk_menu_meta_data_type__default',
    'default_value' => array(),
    'deleted' => 0,
    'description' => NULL,
    'display' => array(
      'default' => array(
        'label' => 'hidden',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
      'diff_standard' => array(
        'label' => 'hidden',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
      'full' => array(
        'label' => 'hidden',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
      'node_teaser' => array(
        'label' => 'hidden',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
      'token' => array(
        'label' => 'hidden',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'gk_menu_meta_data',
    'field_name' => 'domain_610f277c7c5c0fb0e77ab738d',
    'label' => 'Domain',
    'required' => TRUE,
    'settings' => array(
      'entity_translation_sync' => FALSE,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'module' => 'domain_entity',
      'settings' => array(),
      'type' => 'domain_entity_auto_hidden',
      'weight' => 3,
    ),
  );

  // Exported field_instance: 'gk_menu_price_band-gk_menu_price_band_type__default-domain_67eb34df9f870f8321ef9051a'
  $field_instances['gk_menu_price_band-gk_menu_price_band_type__default-domain_67eb34df9f870f8321ef9051a'] = array(
    'bundle' => 'gk_menu_price_band_type__default',
    'default_value' => array(),
    'deleted' => 0,
    'description' => NULL,
    'display' => array(
      'default' => array(
        'label' => 'hidden',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
      'diff_standard' => array(
        'label' => 'hidden',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
      'full' => array(
        'label' => 'hidden',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
      'node_teaser' => array(
        'label' => 'hidden',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
      'teaser' => array(
        'label' => 'hidden',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
      'token' => array(
        'label' => 'hidden',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'gk_menu_price_band',
    'field_name' => 'domain_67eb34df9f870f8321ef9051a',
    'label' => 'Domain',
    'required' => TRUE,
    'settings' => array(
      'entity_translation_sync' => FALSE,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'module' => 'domain_entity',
      'settings' => array(),
      'type' => 'domain_entity_auto_hidden',
      'weight' => 0,
    ),
  );

  // Exported field_instance: 'gk_menu_section-gk_menu_section_type__default-domain_8edf712b4eefc7854ce604d0c'
  $field_instances['gk_menu_section-gk_menu_section_type__default-domain_8edf712b4eefc7854ce604d0c'] = array(
    'bundle' => 'gk_menu_section_type__default',
    'default_value' => array(),
    'deleted' => 0,
    'description' => NULL,
    'display' => array(
      'default' => array(
        'label' => 'hidden',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
      'diff_standard' => array(
        'label' => 'hidden',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
      'full' => array(
        'label' => 'hidden',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
      'node_teaser' => array(
        'label' => 'hidden',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
      'teaser' => array(
        'label' => 'hidden',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
      'token' => array(
        'label' => 'hidden',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'gk_menu_section',
    'field_name' => 'domain_8edf712b4eefc7854ce604d0c',
    'label' => 'Domain',
    'required' => TRUE,
    'settings' => array(
      'entity_translation_sync' => FALSE,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'module' => 'domain_entity',
      'settings' => array(),
      'type' => 'domain_entity_auto_hidden',
      'weight' => 3,
    ),
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Domain');

  return $field_instances;
}
