<?php

/**
 * @file
 * Handle the 'gk_menu view' override task.
 */

/**
 * Specialized implementation of hook_page_manager_task_tasks().
 */
function gk_menus_gk_menu_view_page_manager_tasks() {
  return array(
    'task type' => 'page',
    'title' => t('gk_menu template'),

    'admin title' => t('GKMenu template'),
    'admin description' => t('When enabled, this overrides the default behavior for displaying gk_menu entities at <em>gk-menu/%gk_menu</em>. If you add variants, you may use selection criteria such as entity type or user access to provide different views of gk_menus.'),

    'hook menu alter' => 'gk_menus_gk_menu_view_menu_alter',

    'handler type' => 'context',
    'get arguments' => 'gk_menus_gk_menu_view_get_arguments',
    'get context placeholders' => 'gk_menus_gk_menu_view_get_contexts',

    'disabled' => variable_get('gk_menus_gk_menu_view_disabled', TRUE),
    'enable callback' => 'gk_menus_gk_menu_view_enable',
    'access callback' => 'gk_menus_gk_menu_view_access_check',
  );
}

/**
 * Implements hook_menu_alter().
 */
function gk_menus_gk_menu_view_menu_alter(&$items, $task) {
  if (variable_get('gk_menus_gk_menu_view_disabled', TRUE)) {
    return;
  }

  // Override the gk_menu view handler for our purpose.
  $item = &$items['gk-menu/%gk_menu'];

  if ($item['page callback'] == 'gk_menu_view_page') {
    $item['page callback'] = 'gk_menus_gk_menu_view_page';
    $item['file path'] = $task['path'];
    $item['file'] = $task['file'];
  }
  else {
    // Automatically disable this task if it cannot be enabled.
    variable_set('gk_menus_gk_menu_view_disabled', TRUE);
  }
}

/**
 * Entry point for our overridden gk_menu view.
 */
function gk_menus_gk_menu_view_page($gk_menu) {
  if (!$gk_menu->status && !user_access('administer gk menus content')) {
    return MENU_NOT_FOUND;
  }

  // Load the gk_menu into a context.
  ctools_include('context');
  ctools_include('context-task-handler');

  // We need to mimic Drupal's behavior of setting the gk_menu title here.
  drupal_set_title($gk_menu->title);

  $uri = entity_uri('gk_menu', $gk_menu);

  // Set the gk_menu path as the canonical URL to prevent duplicate content.
  drupal_add_html_head_link(array(
    'rel' => 'canonical',
    'href' => url($uri['path'], $uri['options']),
  ), TRUE);

  // Set the non-aliased path as a default shortlink.
  drupal_add_html_head_link(array(
    'rel' => 'shortlink',
    'href' => url($uri['path'], array_merge($uri['options'], array(
      'alias' => TRUE,
    ))),
  ), TRUE);

  $task = page_manager_get_task('gk_menu_view');
  $contexts = ctools_context_handler_get_task_contexts($task, '', array($gk_menu));

  if ($output = ctools_context_handler_render($task, '', $contexts, array($gk_menu->mid))) {
    return $output;
  }

  // If we get here then no output could be determined after evaluating the
  // available variants. Use the default output instead.
  return gk_menu_view_page($gk_menu);
}

/**
 * Callback to get arguments provided by this task handler.
 */
function gk_menus_gk_menu_view_get_arguments($task, $subtask_id) {
  return array(
    array(
      'keyword' => 'gk_menu',
      'identifier' => t('GKMenu being viewed'),
      'id' => 1,
      'name' => 'entity_id:gk_menu',
      'settings' => array(),
    ),
  );
}

/**
 * Callback to get context placeholders provided by this handler.
 */
function gk_menus_gk_menu_view_get_contexts($task, $subtask_id) {
  $arguments = gk_menus_gk_menu_view_get_arguments($task, $subtask_id);
  return ctools_context_get_placeholders_from_argument($arguments);
}

/**
 * Callback to enable/disable the override from the UI.
 */
function gk_menus_gk_menu_view_enable($cache, $status) {
  variable_set('gk_menus_gk_menu_view_disabled', $status);
}

/**
 * Callback to determine if a page is accessible.
 */
function gk_menus_gk_menu_view_access_check($task, $subtask_id, $contexts) {
  return gk_menu_access('view', reset($contexts)->data);
}
