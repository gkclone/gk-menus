<?php

$plugin = array(
  'title' => 'Menus: Availability',
  'category' => 'GK Menus',
  'single' => TRUE,
);

function gk_menus_gk_menus_availability_content_type_render($subtype, $conf, $args, $context) {
  if (($gk_menu = menu_get_object('gk_menu')) && $meta_data = gk_menus_get_active_meta_data($gk_menu)) {
    if (!empty($meta_data['field_menu_availability'])) {
      $gk_menu_meta_data = entity_get_controller('gk_menu_meta_data')->create(array(
        'type' => 'gk_menu_meta_data_type__default',
        'field_menu_availability' => array(
          LANGUAGE_NONE => $meta_data['field_menu_availability'],
        ),
      ));

      $display = array(
        'label' => 'hidden',
        'settings' => array(
          'showclosed' => FALSE,
        )
      );

      if ($content = field_view_field('gk_menu_meta_data', $gk_menu_meta_data, 'field_menu_availability', $display)) {
        return (object) array(
          'title' => $conf['override_title'] ? $conf['override_title_text'] : t('Availability'),
          'content' => $content,
        );
      }
    }
  }
}

function gk_menus_gk_menus_availability_content_type_edit_form($form, &$form_state) {
  return $form;
}
