<?php

$plugin = array(
  'title' => 'Menus: Key',
  'category' => 'GK Menus',
  'single' => TRUE,
);

function gk_menus_key_content_type_render($subtype, $conf, $args, $context) {
  if (($gk_menu = menu_get_object('gk_menu')) && $gk_menu_items = $gk_menu->getMenuItems()) {
    $items = array();

    foreach ($gk_menu_items as $gk_menu_item) {
      foreach ($gk_menu_item->getTaxonomyTerms() as $term) {
        if (isset($items[$term->tid])) {
          continue;
        }

        $class = drupal_html_class($term->name);

        // Ensure class names don't begin with a number.
        if (is_numeric($class{0})) {
          $class = 'n' . $class;
        }

        $items[$term->tid] = array(
          'data' => $term->name,
          'class' => array($class),
          'id' => 'taxonomy-term-' . $term->tid,
        );
      }
    }

    if (!empty($items)) {
      return (object) array(
        'title' => $conf['override_title'] ? $conf['override_title_text'] : t('Key'),
        'content' => array(
          '#theme' => 'item_list',
          '#items' => $items,
        ),
      );
    }
  }
}

function gk_menus_key_content_type_edit_form($form, &$form_state) {
  return $form;
}

// We need this if we want the title of the plugin to be 'Key', since it matches
// PHP's built in key() and all kinds of nastiness will ensure without it.
function gk_menus_key_content_type_admin_title() {
  return 'Menus: Key';
}
