<?php

$plugin = array(
  'title' => 'Menus: Navigation',
  'category' => 'GK Menus',
  'single' => TRUE,
);

function gk_menus_navigation_content_type_render($subtype, $conf, $args, $context) {
  if ($gk_menus = gk_menu_list()) {
    $active_gk_menu = menu_get_object('gk_menu');
    $items = array();

    foreach ($gk_menus as $gk_menu) {
      // Skip unpublished menus.
      if (!$gk_menu->status) {
        continue;
      }

      $uri = entity_uri('gk_menu', $gk_menu);

      $items[$gk_menu->mid] = array(
        'data' => l($gk_menu->title, $uri['path'], array(
          'attributes' => array(
            'class' => array(
              'GKMenusNavigation-menuLink',
            ),
          ),
        )),
        'data-mid' => $gk_menu->mid,
      );

      // If this gk_menu is the one we're viewing then show an outline of the
      // top-level sections and provide a link to the PDF version.
      if ($active_gk_menu && $active_gk_menu->mid == $gk_menu->mid) {
        $items[$gk_menu->mid]['class'][] = 'is-active';

        // Show an outline of the top-level menu sections?
        if ($conf['show_outline']) {
          $mpbid = NULL;

          if ($gk_menu_price_band = gk_menus_get_active_price_band($gk_menu)) {
            $mpbid = $gk_menu_price_band->mpbid;
          }

          if ($hierarchy = $gk_menu->getHierarchy($mpbid)) {
            $section_items = array();

            foreach ($hierarchy as $section) {
              $title = $section['section']->title;

              $section_items[] = l($title, request_path(), array(
                'fragment' => GKMenuSection::htmlId($section['section']),
              ));
            }

            $items[$gk_menu->mid]['data'] .= theme('item_list', array(
              'items' => $section_items,
              'attributes' => array(
                'class' => array(
                  'GKMenusNavigation-menuSections',
                ),
              ),
            ));
          }
        }

        // PDF version.
        $meta_data = gk_menus_get_active_meta_data($gk_menu);

        if (!empty($meta_data['field_menu_pdf'])) {
          $items[$gk_menu->mid]['data'] .= l(t('Download a PDF'), $uri['path'] . '/pdf', array(
            'attributes' => array(
              'class' => array('GKMenusNavigation-menuPDFLink'),
            ),
          ));
        }
      }
    }

    return (object) array(
      'title' => $conf['override_title'] ? $conf['override_title_text'] : t('Menus'),
      'content' => array(
        '#theme' => 'item_list',
        '#items' => $items,
        '#attributes' => array(
          'class' => array(
            'GKMenusNavigation-menus',
          ),
        ),
      ),
    );
  }
}

function gk_menus_navigation_content_type_edit_form($form, &$form_state) {
  $conf = $form_state['conf'];

  $form['show_outline'] = array(
    '#type' => 'checkbox',
    '#title' => 'Show outline',
    '#description' => 'Show an outline of the top-level sections in the menu being viewed.',
    '#default_value' => isset($conf['show_outline']) ? $conf['show_outline'] : 0,
  );

  return $form;
}

function gk_menus_navigation_content_type_edit_form_submit($form, &$form_state) {
  form_state_values_clean($form_state);
  $form_state['conf'] = $form_state['values'];
}
