<?php

$plugin = array(
  'title' => 'Menus: Current location',
  'category' => 'GK Menus',
  'single' => TRUE,
);

function gk_menus_current_location_content_type_render($subtype, $conf, $args, $context) {
  if ($current_location = gk_locations_get_current_location()) {
    $message = t('You are currently viewing the menus for !location', array(
      '!location' => l(t($current_location->title), 'node/' . $current_location->nid),
    ));
    $first_option_text = 'Choose a different location';
  }
  else {
    $message = t('You are currently viewing a sample menu.');
    $first_option_text = 'Choose a location';
  }

  $form_state = array(
    'gk_locations' => array(
      'get_items_options' => array(
        'exclude_current_location' => TRUE,
      ),
      'first_option_text' => $first_option_text,
    ),
  );

  return (object) array(
    'title' => $conf['override_title'] ? $conf['override_title_text'] : '',
    'content' => array(
      'message' => array(
        '#theme_wrappers' => array('container'),
        '#attributes' => array(
          'class' => array('gk-menus-current-location'),
        ),
        '#markup' => $message,
      ),
      'locations' => drupal_build_form('gk_menus_choose_location_form', $form_state),
    ),
  );
}

function gk_menus_current_location_content_type_edit_form($form, &$form_state) {
  return $form;
}

/**
 * A form with a dropdown of locations. This is basically a copy of the form in
 * the locations module, except with a submit handler for altering the redirect.
 */
function gk_menus_choose_location_form($form, &$form_state) {
  module_load_include('inc', 'gk_locations', 'plugins/content_types/choose_form');
  $form += drupal_retrieve_form('gk_locations_choose_form', $form_state);

  // Make use of the original submit handler.
  $form['#submit'] = array(
    'gk_locations_choose_form_submit',
    'gk_menus_choose_location_form_submit',
  );

  return $form;
}

/**
 * Submit handler for the locations dropdown form.
 */
function gk_menus_choose_location_form_submit($form, &$form_state) {
  // If we're looking at a menu view page then send the user back to that page,
  // otherwise send them to /menus.
  $form_state['redirect'] = 'menus';

  if ($gk_menu = menu_get_object('gk_menu')) {
    $uri = entity_uri('gk_menu', $gk_menu);
    $form_state['redirect'] = $uri['path'];
  }
}
