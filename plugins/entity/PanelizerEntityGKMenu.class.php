<?php

/**
 * @file
 * Class for the gk_menu entity Panelizer plugin.
 */

/**
 * Handles GKMenu specific functionality for Panelizer.
 */
class PanelizerEntityGKMenu extends PanelizerEntityDefault {
  public $uses_page_manager = TRUE;

  public function entity_access($op, $entity) {
    return gk_menu_access($op, $entity);
  }

  public function entity_save($entity) {
    entity_save('gk_menu', $entity);
  }

  public function entity_identifier($entity) {
    return t('This GKMenu');
  }

  public function entity_bundle_label() {
    return t('GKMenu type');
  }

  public function settings_form(&$form, &$form_state) {
    parent::settings_form($form, $form_state);

    // Display a message if the page manager task for overriding gk_menu output
    // hasn't been enabled, or if the panelizer variant for this override hasn't
    // been created.
    $warn = FALSE;

    foreach ($this->plugin['bundles'] as $info) {
      if (!empty($info['status']) && !empty($info['view modes']['page_manager']['status'])) {
        $warn = TRUE;
        break;
      }
    }

    if ($warn) {
      $task = page_manager_get_task('gk_menu_view');

      if (!empty($task['disabled'])) {
        drupal_set_message('The gk_menu template page is currently not enabled in page manager. You must enable this for Panelizer to be able to panelize nodes using the "Full page override" view mode.', 'warning');
      }

      $handler = page_manager_load_task_handler($task, '', 'gk_menu_view_panelizer');

      if (!empty($handler->disabled)) {
        drupal_set_message('The panelizer variant on the node template page is currently not enabled in page manager. You must enable this for Panelizer to be able to panelize nodes using the "Full page override" view mode.', 'warning');
      }
    }
  }

  function get_default_display($bundle, $view_mode) {
    $display = parent::get_default_display($bundle, $view_mode);

    // Add the menu title to the display since we can't get that automatically.
    $display->title = '%gk_menu:title';

    return $display;
  }

  /**
   * Implements a delegated hook_default_page_manager_handlers().
   */
  public function hook_default_page_manager_handlers(&$handlers) {
    $handler = new stdClass;
    $handler->disabled = FALSE;
    $handler->api_version = 1;
    $handler->name = 'gk_menu_view_panelizer';
    $handler->task = 'gk_menu_view';
    $handler->subtask = '';
    $handler->handler = 'panelizer_node';
    $handler->weight = -100;
    $handler->conf = array(
      'title' => t('GKMenu panelizer'),
      'context' => 'argument_entity_id:gk_menu_1',
      'access' => array(),
    );
    $handlers['gk_menu_view_panelizer'] = $handler;

    return $handlers;
  }

  /**
   * Implements a delegated hook_form_alter().
   */
  public function hook_form_alter(&$form, &$form_state, $form_id) {
    // Add panelizer settings to the gk_menu_type entity form.
    if ($form_id == 'gk_menu_type_form' && !empty($form['type']['#default_value'])) {
      $this->add_bundle_setting_form($form, $form_state, $form['type']['#default_value'], array('type'));
    }
  }
}
