<?php

/**
 * @file
 * Definition of the gk_menu entity Panelizer plugin.
 */

$plugin = array(
  'handler' => 'PanelizerEntityGKMenu',
  'entity path' => 'gk-menu/%gk_menu',
  'uses page manager' => TRUE,
  'hooks' => array(
    'menu' => TRUE,
    'admin_paths' => TRUE,
    'permission' => TRUE,
    'panelizer_defaults' => TRUE,
    'default_page_manager_handlers' => TRUE,
    'form_alter' => TRUE,
  ),
);
