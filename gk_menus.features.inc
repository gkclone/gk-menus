<?php
/**
 * @file
 * gk_menus.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function gk_menus_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "panelizer" && $api == "panelizer") {
    return array("version" => "1");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_default_gk_menu_item_type().
 */
function gk_menus_default_gk_menu_item_type() {
  $items = array();
  $items['gk_menu_item_type__default'] = entity_import('gk_menu_item_type', '{
    "type" : "gk_menu_item_type__default",
    "label" : "Default",
    "description" : "Default menu item type."
  }');
  return $items;
}

/**
 * Implements hook_default_gk_menu_meta_data_type().
 */
function gk_menus_default_gk_menu_meta_data_type() {
  $items = array();
  $items['gk_menu_meta_data_type__default'] = entity_import('gk_menu_meta_data_type', '{
    "type" : "gk_menu_meta_data_type__default",
    "label" : "Default",
    "description" : "Default menu meta data type."
  }');
  return $items;
}

/**
 * Implements hook_default_gk_menu_price_band_type().
 */
function gk_menus_default_gk_menu_price_band_type() {
  $items = array();
  $items['gk_menu_price_band_type__default'] = entity_import('gk_menu_price_band_type', '{
    "type" : "gk_menu_price_band_type__default",
    "label" : "Default",
    "description" : "Default menu price band type."
  }');
  return $items;
}

/**
 * Implements hook_default_gk_menu_section_type().
 */
function gk_menus_default_gk_menu_section_type() {
  $items = array();
  $items['gk_menu_section_type__default'] = entity_import('gk_menu_section_type', '{
    "type" : "gk_menu_section_type__default",
    "label" : "Default",
    "description" : "Default menu section type."
  }');
  return $items;
}

/**
 * Implements hook_default_gk_menu_type().
 */
function gk_menus_default_gk_menu_type() {
  $items = array();
  $items['gk_menu_type__default'] = entity_import('gk_menu_type', '{
    "type" : "gk_menu_type__default",
    "label" : "Default",
    "description" : "Default menu type."
  }');
  return $items;
}
