<?php
/**
 * @file
 * gk_menus.panelizer.inc
 */

/**
 * Implements hook_panelizer_defaults().
 */
function gk_menus_panelizer_defaults() {
  $export = array();

  $panelizer = new stdClass();
  $panelizer->disabled = FALSE; /* Edit this to true to make a default panelizer disabled initially */
  $panelizer->api_version = 1;
  $panelizer->name = 'gk_menu:gk_menu_type__default:default';
  $panelizer->title = 'Default';
  $panelizer->panelizer_type = 'gk_menu';
  $panelizer->panelizer_key = 'gk_menu_type__default';
  $panelizer->no_blocks = FALSE;
  $panelizer->css_id = '';
  $panelizer->css = '';
  $panelizer->pipeline = 'ipe';
  $panelizer->contexts = array();
  $panelizer->relationships = array();
  $panelizer->access = array();
  $panelizer->view_mode = 'page_manager';
  $panelizer->css_class = '';
  $panelizer->title_element = 'H2';
  $panelizer->link_to_entity = TRUE;
  $panelizer->extra = array();
  $display = new panels_display();
  $display->layout = '2_columns_25_75';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'primary' => NULL,
      'secondary' => NULL,
      'top' => NULL,
      'bottom' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = '%gk_menu:title';
  $display->uuid = 'bf462690-288a-439c-b431-0602accfada3';
  $display->content = array();
  $display->panels = array();
    $pane = new stdClass();
    $pane->pid = 'new-ec6118d8-3c8a-46c3-b082-dd391ddd3f74';
    $pane->panel = 'primary';
    $pane->type = 'pane_content_header';
    $pane->subtype = 'pane_content_header';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array();
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = 'ec6118d8-3c8a-46c3-b082-dd391ddd3f74';
    $display->content['new-ec6118d8-3c8a-46c3-b082-dd391ddd3f74'] = $pane;
    $display->panels['primary'][0] = 'new-ec6118d8-3c8a-46c3-b082-dd391ddd3f74';
    $pane = new stdClass();
    $pane->pid = 'new-ad29bf16-48fc-4549-9353-e6e8497d3535';
    $pane->panel = 'primary';
    $pane->type = 'current_location';
    $pane->subtype = 'current_location';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 0,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 1;
    $pane->locks = array();
    $pane->uuid = 'ad29bf16-48fc-4549-9353-e6e8497d3535';
    $display->content['new-ad29bf16-48fc-4549-9353-e6e8497d3535'] = $pane;
    $display->panels['primary'][1] = 'new-ad29bf16-48fc-4549-9353-e6e8497d3535';
    $pane = new stdClass();
    $pane->pid = 'new-dbeabb17-49cb-497f-ab4c-354e545f961f';
    $pane->panel = 'primary';
    $pane->type = 'entity_view';
    $pane->subtype = 'gk_menu';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'view_mode' => 'full',
      'context' => 'panelizer',
      'override_title' => 0,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 2;
    $pane->locks = array();
    $pane->uuid = 'dbeabb17-49cb-497f-ab4c-354e545f961f';
    $display->content['new-dbeabb17-49cb-497f-ab4c-354e545f961f'] = $pane;
    $display->panels['primary'][2] = 'new-dbeabb17-49cb-497f-ab4c-354e545f961f';
    $pane = new stdClass();
    $pane->pid = 'new-689c677b-5831-4b0c-bcad-180cd703aaad';
    $pane->panel = 'secondary';
    $pane->type = 'navigation';
    $pane->subtype = 'navigation';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 0,
      'override_title_text' => '',
      'filter_by_current_location' => 0,
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '689c677b-5831-4b0c-bcad-180cd703aaad';
    $display->content['new-689c677b-5831-4b0c-bcad-180cd703aaad'] = $pane;
    $display->panels['secondary'][0] = 'new-689c677b-5831-4b0c-bcad-180cd703aaad';
    $pane = new stdClass();
    $pane->pid = 'new-afbc1409-1815-4481-97f7-755316788cda';
    $pane->panel = 'secondary';
    $pane->type = 'key';
    $pane->subtype = 'key';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 0,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 1;
    $pane->locks = array();
    $pane->uuid = 'afbc1409-1815-4481-97f7-755316788cda';
    $display->content['new-afbc1409-1815-4481-97f7-755316788cda'] = $pane;
    $display->panels['secondary'][1] = 'new-afbc1409-1815-4481-97f7-755316788cda';
    $pane = new stdClass();
    $pane->pid = 'new-05d405a2-b74b-4d63-8413-11416cf49874';
    $pane->panel = 'secondary';
    $pane->type = 'gk_menus_availability';
    $pane->subtype = 'gk_menus_availability';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 0,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 2;
    $pane->locks = array();
    $pane->uuid = '05d405a2-b74b-4d63-8413-11416cf49874';
    $display->content['new-05d405a2-b74b-4d63-8413-11416cf49874'] = $pane;
    $display->panels['secondary'][2] = 'new-05d405a2-b74b-4d63-8413-11416cf49874';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = '0';
  $panelizer->display = $display;
  $export['gk_menu:gk_menu_type__default:default'] = $panelizer;

  return $export;
}
