<?php

/**
 *
 */
class GKMenusLocation implements GKMenusHasMetaDataEntityInterface {
  /**
   *
   */
  private $gk_menu;

  /**
   *
   */
  private $location_node;

  /**
   *
   */
  private static $association_records;

  /**
   * Constructor.
   */
  public function __construct($gk_menu, $location_node) {
    $this->gk_menu = $gk_menu;
    $this->location_node = $location_node;
  }

  /**
   * Getter for $this->gk_menu.
   */
  public function getMenu() {
    return $this->gk_menu;
  }

  /**
   * Getter for $this->location_node.
   */
  public function getLocationNode() {
    return $this->location_node;
  }

  /**
   * Implements GKMenusHasMetaDataEntityInterface::getMetaDataEntity().
   */
  public function getMetaDataEntity() {
    // Load the meta data entity associated with this menu/location combination.
    if ($association = self::loadAssociationRecord($this->getMenu(), $this->getLocationNode())) {
      return gk_menu_meta_data_load($association->mmdid);
    }
  }

  /**
   * Implements GKMenusHasMetaDataEntityInterface::getMetaDataParentEntity().
   */
  public function getMetaDataParentEntity() {
    // Load the price band associated with this menu/location combination.
    if ($association = self::loadAssociationRecord($this->getMenu(), $this->getLocationNode())) {
      return gk_menu_price_band_load($association->mpbid);
    }
  }

  /**
   * Implements GKMenusHasMetaDataEntityInterface::getMetaData().
   */
  public function getMetaData() {
    if (($gk_menu_meta_data = $this->getMetaDataEntity()) && $meta_data = GKMenuMetaData::getMetaData($gk_menu_meta_data, $this)) {
      return $meta_data;
    }
  }

  /**
   * Helper method for loading a record from the price band/location node table.
   */
  public static function loadAssociationRecord($gk_menu, $location_node) {
    if (!isset(self::$association_records[$gk_menu->mid][$location_node->nid])) {
      $query = db_select('gk_menu_price_band__node_location', 'pbnl')
        ->fields('pbnl')->condition('nid', $location_node->nid);

      $query->join('gk_menu_price_band', 'pb', 'pbnl.mpbid = pb.mpbid');
      $query->condition('pb.mid', $gk_menu->mid);

      self::$association_records[$gk_menu->mid][$location_node->nid] = $query->execute()->fetchObject();
    }

    return self::$association_records[$gk_menu->mid][$location_node->nid];
  }
}
